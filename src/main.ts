import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import * as dotenv from 'dotenv';
import { ValidationPipe } from '@nestjs/common';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
dotenv.config();

// eslint-disable-next-line @typescript-eslint/no-var-requires
const packageInfo = require('./../package.json');

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.enableCors();
  app.useGlobalPipes(
    new ValidationPipe({
      whitelist: true,
    }),
  );

  if (
    process.env.NODE_ENV === 'dev' ||
    process.env.NODE_ENV === 'development'
  ) {
    const config = new DocumentBuilder()
      .setTitle('OpenSCN - Web API')
      .setDescription('The web interface for the OpenSCN platform')
      .setVersion(packageInfo.version)
      .addTag('OpenSCN')
      .build();
    const document = SwaggerModule.createDocument(app, config);
    SwaggerModule.setup('api', app, document);
  }

  await app.listen(3000);
}

bootstrap().catch((err) => console.error(err));

// process.on('uncaughtException', function (err) {
//   process.exit(1);
// });
