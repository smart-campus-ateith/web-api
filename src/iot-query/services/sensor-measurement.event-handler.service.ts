import { Injectable, Logger } from '@nestjs/common';
import { SensorMeasurementService } from './sensor-measurement.service';
import {
  ApplicationEventSubject,
  EventHandlerService,
  SensorMeasurementCreatedEvent,
  SensorMeasurementRemovedEvent,
  SensorMeasurementUpdatedEvent,
} from '../../common';
import { OnEvent } from '@nestjs/event-emitter';
import { SensorMeasurementEntity } from '../entities/sensor-measurement.entity';

@Injectable()
export class SensorMeasurementEventHandlerService extends EventHandlerService {
  constructor(
    private readonly _sensorMeasurementService: SensorMeasurementService,
  ) {
    super(new Logger(`IoTQuery/${SensorMeasurementEventHandlerService.name}`));
  }

  @OnEvent(ApplicationEventSubject.SensorMeasurementCreated)
  async onSensorMeasurementCreatedEvent(
    event: SensorMeasurementCreatedEvent['payload'],
  ) {
    this.logEvent(ApplicationEventSubject.SensorMeasurementCreated, event);

    try {
      await this._sensorMeasurementService.save({
        owner: event.owner,
        identifier: event.measurement,
        sensor: event.sensor,
        actuator: event.actuator,
        version: event.version,
        isActive: event.isActive,
        isProduction: event.isProduction,
        additionalType: event.additionalType,
      });
    } catch (err) {
      this._logger.error(err.stack ?? err);
    }
  }

  @OnEvent(ApplicationEventSubject.SensorMeasurementUpdated)
  async onSensorMeasurementUpdatedEvent(
    event: SensorMeasurementUpdatedEvent['payload'],
  ) {
    this.logEvent(ApplicationEventSubject.SensorMeasurementUpdated, event);

    let measurement: SensorMeasurementEntity;

    try {
      measurement = await this._sensorMeasurementService.findOne({
        where: {
          identifier: event.measurement,
        },
      });
    } catch (err) {
      this._logger.error(err.stack ?? err);
    }

    if (!measurement) {
      this._logger.error(
        `${this._sensorMeasurementService.name} with identifier ${event.measurement} was not found`,
      );
      return;
    } else if (measurement.version !== event.version - 1) {
      this._logger.warn(
        `${this._sensorMeasurementService.name} with identifier is being updated from version ${measurement.version} to ${event.version}`,
      );
    }

    this._sensorMeasurementService
      .save({
        ...measurement,
        isActive: event.isActive,
        isProduction: event.isProduction,
        version: event.version,
      })
      .catch((err) => this._logger.error(err.stack ?? err));
  }

  @OnEvent(ApplicationEventSubject.SensorMeasurementRemoved)
  async onSensorMeasurementRemovedEvent(
    event: SensorMeasurementRemovedEvent['payload'],
  ) {
    this.logEvent(ApplicationEventSubject.SensorMeasurementRemoved, event);

    try {
      await this._sensorMeasurementService.softRemove({
        identifier: event.measurement,
      });
    } catch (err) {
      this._logger.error(err.stack ?? err);
    }
  }
}
