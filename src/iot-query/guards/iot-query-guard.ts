import {
  BadRequestException,
  CanActivate,
  ExecutionContext,
  Injectable,
  InternalServerErrorException,
  Logger,
  NotFoundException,
} from '@nestjs/common';
import { EncapsulatedError, EncapsulatedErrorService } from '../../common';
import { SensorMeasurementService } from '../services/sensor-measurement.service';
import { SensorMeasurementEntity } from '../entities/sensor-measurement.entity';

@Injectable()
export class IotQueryGuard implements CanActivate {
  private readonly _logger = new Logger(`IoTQuery/${IotQueryGuard.name}`);

  constructor(
    private readonly _sensorMeasurementService: SensorMeasurementService,
    private readonly _encapsulatedErrorService: EncapsulatedErrorService,
  ) {}

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const request = context.switchToHttp().getRequest();

    if (!request.query) {
      const error = new BadRequestException('Request query can not be empty');
      throw this._encapsulatedErrorService.formatError(
        this._logger,
        new EncapsulatedError(error, error),
      );
    }

    let measurement: SensorMeasurementEntity;

    try {
      measurement = await this._sensorMeasurementService.findOne({
        where: {
          identifier: request.query.measurement,
        },
      });
    } catch (err) {
      throw this._encapsulatedErrorService.formatError(
        this._logger,
        new EncapsulatedError(err, new InternalServerErrorException()),
      );
    }

    if (!measurement) {
      const error = new NotFoundException(
        `SensorMeasurementEntity for measurement ${request.query.measurement} was not found`,
      );
      throw this._encapsulatedErrorService.formatError(
        this._logger,
        new EncapsulatedError(error, error),
      );
    }

    return measurement.owner === request.user.identifier;
  }
}
