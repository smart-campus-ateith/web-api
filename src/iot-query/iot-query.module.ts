import { Module } from '@nestjs/common';
import { IotQueryController } from './controllers/iot-query.controller';
import { IotQueryService } from './services/iot-query.service';
import { TimeSeriesModule } from '../time-series/time-series.module';
import { CommonModule } from '../common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { IConfiguration } from '../config/config.inteface';
import { UsersModule } from '../users/users.module';
import { AuthModule } from '../auth/auth.module';
import { SensorMeasurementService } from './services/sensor-measurement.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { SensorMeasurementEntity } from './entities/sensor-measurement.entity';
import { SensorMeasurementEventHandlerService } from './services/sensor-measurement.event-handler.service';

@Module({
  imports: [
    ConfigModule,
    CommonModule,
    UsersModule,
    AuthModule,
    TypeOrmModule.forFeature([SensorMeasurementEntity]),
    TimeSeriesModule.registerAsync({
      useFactory: (configService: ConfigService) => {
        const influxDbOptions: IConfiguration['influx'] =
          configService.get('influx');
        return {
          url: influxDbOptions.url,
          org: influxDbOptions.org,
          bucket: influxDbOptions.bucket,
          token: influxDbOptions.token,
        };
      },
      inject: [ConfigService],
    }),
  ],
  providers: [
    IotQueryService,
    SensorMeasurementService,
    SensorMeasurementEventHandlerService,
  ],
  controllers: [IotQueryController],
  exports: [],
})
export class IotQueryModule {}
