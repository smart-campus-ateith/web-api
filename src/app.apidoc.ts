/**
 *
 * HEADERS
 *
 */

/**
 *
 * @apiDefine authorizedHeader
 *
 * @apiHeader {String} authorization Bearer <token>
 * @apiHeader {String} Content-Type application/json
 *
 * @apiHeaderExample {json} Authorized
 * {
 *  "Content-Type": "application/json"
 *  "Authorization": "Bearer <token>"
 * }
 *
 */

/**
 *
 * @apiDefine unauthorized
 *
 * @apiHeader {String} Content-Type application/json
 *
 * @apiHeaderExample {json} Unauthorized
 * {
 *  "Content-Type": "application/json"
 * }
 *
 */

/**
 *
 * ERRORS
 *
 */

/**
 *
 * @apiDefine badFormatError
 *
 * @apiError (Error - 4xx) {400} BadRequestError Bad request Error
 *
 * @apiErrorExample {json} 400 Bad Request Example Response
HTTP/1.1 400 Not Found Error
{
  "statusCode": 400,
  "message": [
    "username must contain only letters and numbers",
    "lastName must contain only letters (a-zA-Z)"
  ],
  "error": "Bad Request"
}
 *
 */

/**
 *
 * @apiDefine notAuthorizedError
 *
 * @apiError (Error - 4xx) {401} notAuthorizedError Not Authorized Error
 *
 * @apiErrorExample {json} 401 Not Authorized Example Response
 *  HTTP/1.1 401 Not Found Error
 * {
 *  "statusCode": 401,
 *  "error": "Unauthorized"
 * }
 *
 */

/**
 *
 * @apiDefine notFoundError
 *
 * @apiError (Error - 4xx) {404} NotFoundError Not Found Error
 *
 * @apiErrorExample {json} 404 Not Found Example Response
HTTP/1.1 404 Not Found Error
{
  "statusCode": 404,
  "message": "User not found.",
  "error": "Not Found"
}
 */

/**
 *
 * @apiDefine conflictError
 *
 * @apiError (Error - 4xx) {409} Conflict Conflict
 *
 * @apiErrorExample {json} 409 Conflict
HTTP/1.1 404 Conflict
{
  "statusCode": 409,
  "message": [
    "name must be unique"
  ],
  "error": "Conflict
}
 *
 */

/**
 *
 * @apiDefine internalError
 *
 * @apiError (Error - 5xx) {500} InternalServerError Internal Server Error
 *
 * @apiErrorExample {json} 500  Internal Error Example Response
 *  HTTP/1.1 500 Internal Server Error
 *  {
 *   "type": "error",
 *   "code": 500,
 *   "message": "Internal Server Error"
 *  }
 *
 */

/**
 *
 * @apiDefine notImplementedError
 *
 * @apiError (Error - 5xx) {501} NotImplementedError Not Implemented Error
 *
 * @apiErrorExample {json} 500  Not Implemented Error Example Response
 *  HTTP/1.1 500 Not Implemented Error
 *  {
 *   "type": "error",
 *   "code": 501,
 *   "message": "Not Implemented Error"
 *  }
 *
 */

/**
 *
 * @apiDefine defaultListUrlParams
 *
 * @apiParam (URL - Query) {number} [page=1] The page of the resource list. -1 for all instances
 * @apiParam (URL - Query) {number} [perPage=20] The number of instances per page of the resource list
 * @apiParam (URL - Query) {string} [orderBy=id] The field of the resource to order the list
 * @apiParam (URL - Query) {string="ACS","DESC"} [order=ASC] The order of the list
 *
 */
