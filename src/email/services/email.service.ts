import { Injectable, Logger } from '@nestjs/common';
import { MailerService } from '@nestjs-modules/mailer';
import { EmailOptions } from '../email.types';
import * as path from 'path';

@Injectable()
export class EmailService {
  private readonly _logger = new Logger(EmailService.name);

  constructor(private readonly _mailService: MailerService) {}

  sendEmail<T extends EmailOptions>(options: T) {
    this._mailService
      .sendMail({
        to: options.to,
        from: options.from,
        template: path.resolve('templates', `${options.template}.pug`),
        subject: options.subject,
        context: options.context,
        textEncoding: 'quoted-printable',
      })
      .then(() =>
        this._logger.verbose(`Sent email ${options.template} to ${options.to}`),
      )
      .catch((err) => this._logger.error(err?.stack ?? err));
  }
}
