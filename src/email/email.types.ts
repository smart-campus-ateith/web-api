export enum EmailType {
  'AuthAccountCreated' = 'auth.user.account.created',
  'AuthInviteUser' = 'auth.user.invite',
  'AuthResetPassword' = 'auth.user.account.reset-password',
}

export enum EmailSubject {
  'AuthAccountCreated' = 'Your account has been created',
  'AuthInviteUser' = 'You have been invited to OpenSCN',
  'AuthResetPassword' = 'Reset your password',
}

export interface AccountCreatedContext {
  clientUrl: string;
  assetsPath: string;
}

export interface InviteUserContext {
  clientUrl: string;
  clientPath: string;
  clientQuery: string;
  assetsPath: string;
}

export interface ResetPasswordContext {
  clientUrl: string;
  clientPath: string;
  clientQuery: string;
  assetsPath: string;
  token: string;
}

export interface EmailOptions {
  to: string;
  from?: string;
  subject: EmailSubject;
  template: EmailType;
  context?: unknown;
}

export interface AccountCreatedEmail extends EmailOptions {
  template: EmailType.AuthAccountCreated;
  subject: EmailSubject.AuthAccountCreated;
  context: AccountCreatedContext;
}

export interface AuthInviteUserEmail extends EmailOptions {
  template: EmailType.AuthInviteUser;
  subject: EmailSubject.AuthInviteUser;
  context: InviteUserContext;
}

export interface AuthResetPasswordEmail extends EmailOptions {
  template: EmailType.AuthResetPassword;
  subject: EmailSubject.AuthResetPassword;
  context: ResetPasswordContext;
}
