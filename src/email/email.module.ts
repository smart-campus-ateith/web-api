import { Module } from '@nestjs/common';
import { MailerModule, MailerOptions } from '@nestjs-modules/mailer';
import * as path from 'path';
import { PugAdapter } from '@nestjs-modules/mailer/dist/adapters/pug.adapter';
import { EmailService } from './services/email.service';
import { ConfigService } from '@nestjs/config';
import { IConfiguration } from '../config/config.inteface';

@Module({
  imports: [
    MailerModule.forRootAsync({
      useFactory: (configService: ConfigService) => {
        const config: IConfiguration['email'] = configService.get('email');

        const configuration: MailerOptions = {
          transport: {
            host: config.host,
            port: Number(config.port),
            ignoreTLS: config.ignoreTLS,
            secure: config.secure,
          },
          defaults: {
            from: configService.get('email').defaultSender,
          },
          preview: false,
          template: {
            dir: path.resolve(__dirname, 'templates'),
            adapter: new PugAdapter({
              inlineCssEnabled: false,
            }),
            options: {
              strict: true,
              partials: {
                dir: path.join(__dirname, 'templates/partials'),
                options: {
                  strict: true,
                },
              },
            },
          },
        };

        if (config.password && config.user) {
          // eslint-disable-next-line @typescript-eslint/ban-ts-comment
          // @ts-ignore
          configuration.transport.auth = {
            user: config.user,
            pass: config.password,
          };
        }

        return configuration;
      },
      inject: [ConfigService],
    }),
  ],
  providers: [EmailService],
  exports: [EmailService],
})
export class EmailModule {}
