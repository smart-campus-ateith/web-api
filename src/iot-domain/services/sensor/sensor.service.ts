import { Injectable, Logger } from '@nestjs/common';
import {
  ApplicationEventSubject,
  EntityService,
  SensorMeasurementActivatedEvent,
} from '../../../common';
import { SensorEntity } from '../../entities/sensor.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { DeepPartial, FindOptionsWhere, Repository } from 'typeorm';
import { SensorCreatedEvent } from '../../../common/events/sensor-created.event';
import { ApplicationEventsService } from '../../../common/services/application-events.service';
import { SensorUpdatedEvent } from '../../../common/events/sensor-updated.event';
import { SensorMeasurementService } from '../sensor-measeurement/sensor-measurement.service';
import { SensorMeasurementEntity } from '../../entities/sensor-measurement.entity';
import { SensorLabelEntity } from '../../entities/sensor-label.entity';
import { IoTAlertConfigurationService } from '../iot-alert-configuration/iot-alert-configuration.service';

@Injectable()
export class SensorService extends EntityService<SensorEntity> {
  private readonly _logger = new Logger(`IoTDomain/${SensorService.name}`);

  constructor(
    @InjectRepository(SensorEntity)
    private readonly sensorRepository: Repository<SensorEntity>,
    private readonly _applicationEventsService: ApplicationEventsService,
    private readonly _sensorMeasurementService: SensorMeasurementService,
    private readonly _iotAlertConfigurationService: IoTAlertConfigurationService,
  ) {
    super(SensorEntity.name);
    this.repository = sensorRepository;
  }

  async save(options: DeepPartial<SensorEntity>) {
    const result = await super.save(options);

    const event: SensorCreatedEvent = {
      subject: ApplicationEventSubject.SensorCreated,
      payload: {
        owner: result.owner.identifier,
        identifier: result.identifier,
        valueType: result.valueType,
        measurementUnit: result.measurementUnit,
        parentDevice: result.parentDevice?.identifier,
      },
    };
    this._applicationEventsService.emit(event);

    try {
      const measurement = await this._sensorMeasurementService.createAndSave({
        sensor: result,
        owner: result.owner,
        name: 'default',
        description: 'Default measurement',
        isActive: true,
      });
    } catch (err) {
      this._logger.error(err.stack ?? err);
    }

    return result;
  }

  async update(id, fields) {
    const updateResult = await super.update(id, fields);

    if (updateResult?.affected > 0) {
      const result = await this.sensorRepository.findOne({
        where: {
          id: id,
        },
      });

      const event: SensorUpdatedEvent = {
        subject: ApplicationEventSubject.SensorUpdated,
        payload: {
          identifier: result.identifier,
          parentDevice: result?.parentDevice?.identifier,
        },
      };

      this._applicationEventsService.emit(event);
    }

    return updateResult;
  }

  async softRemove(id: number | FindOptionsWhere<SensorEntity>) {
    let sensor: SensorEntity;

    if (typeof id === 'number') {
      sensor = await this.findOne({
        where: {
          id: id,
        },
        relations: ['owner'],
      });
    } else {
      sensor = await this.findOne({
        where: id,
        relations: ['owner'],
      });
    }

    await this._iotAlertConfigurationService.softRemove({
      sensor: {
        id: sensor.id,
      },
    });

    return super.softRemove(id);
  }

  async addLabel(sensor: SensorEntity, label: SensorLabelEntity) {
    const labels = sensor.labels;
    labels.push(label);

    await this.repository.manager.save(sensor);
  }

  async removeLabel(sensor: SensorEntity, label: SensorLabelEntity) {
    sensor.labels = sensor.labels.filter(
      (l) => l.identifier !== label.identifier,
    );
    await this.repository.manager.save(sensor);
  }

  async updateLabels(sensor: SensorEntity, labels: SensorLabelEntity[]) {
    // const [toAdd, toRemove ] = this.compareLabels(sensor, labels);
    sensor.labels = labels;
    await this.repository.save(sensor);
  }

  compareLabels(sensor: SensorEntity, labels: SensorLabelEntity[]) {
    const existing: Record<string, number> = {};
    const toAdd = [];
    const toRemove = [];

    for (let i = 0; i < sensor.labels.length; i++) {
      existing[sensor.labels[i].identifier] = 0;
    }

    for (let i = 0; i < labels.length; i++) {
      const key = labels[i].identifier;
      if (typeof existing[key] !== 'undefined') {
        existing[key]++;
      } else {
        toAdd.push(labels[i]);
      }
    }

    for (let i = 0; i < sensor.labels.length; i++) {
      const key = sensor.labels[i].identifier;
      if (existing[key] === 0) {
        toRemove.push(sensor.labels[i]);
      }
    }

    return [toAdd, toRemove];
  }
}
