import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { EntityService } from '../../../common';
import { IotActuatorTriggerActionEntity } from '../../entities/iot-actuator-trigger-action.entity';

@Injectable()
export class IotActuatorTriggerActionService extends EntityService<IotActuatorTriggerActionEntity> {
  constructor(
    @InjectRepository(IotActuatorTriggerActionEntity)
    private readonly _iotActuatorTriggerActionRepository: Repository<IotActuatorTriggerActionEntity>,
  ) {
    super(IotActuatorTriggerActionEntity.name);
    this.repository = _iotActuatorTriggerActionRepository;
  }
}
