import { Injectable, Logger } from '@nestjs/common';
import {
  ApplicationEventsService,
  ApplicationEventSubject,
  EventHandlerService,
  IIoTAlertCreatedEvent,
} from '../../../common';
import { OnEvent } from '@nestjs/event-emitter';
import { IoTAlertService } from '../iot-alert/iot-alert.service';
import { IotActuatorTriggerConfigurationEntity } from 'src/iot-domain/entities/iot-actuator-trigger-configuration.entity';
import { IotActuatorTriggerConfigurationService } from './iot-actuator-trigger-configuration.service';
import { IotActuatorService } from '../iot-actuator/iot-actuator.service';
import { IotActuatorTriggerActionService } from '../iot-actuator-trigger/iot-actuator-trigger.service';

@Injectable()
export class IotActuatorTriggerConfigurationHandlerService extends EventHandlerService {
  constructor(
    private readonly _iotActuatorTriggerConfigurationService: IotActuatorTriggerConfigurationService,
    private readonly _iotAlertService: IoTAlertService,
    private readonly _iotActuatorService: IotActuatorService,
    private readonly _iotActuatorTriggerService: IotActuatorTriggerActionService,

    private readonly _applicationEventsService: ApplicationEventsService,
  ) {
    super(
      new Logger(
        `IoTDomain/${IotActuatorTriggerConfigurationHandlerService.name}`,
      ),
    );
  }

  @OnEvent(ApplicationEventSubject.IoTAlertCreated)
  async onIoTAlertCreated(data: IIoTAlertCreatedEvent['payload']) {
    this.logEvent(
      ApplicationEventSubject.IoTTaskAlertCheckResultReceived,
      data,
    );

    const alert = await this._iotAlertService.findOne({
      where: {
        identifier: data.alert,
      },
      relations: ['alertConfiguration', 'alertConfiguration.sensor'],
    });

    let actuatorTriggerConfiguration: IotActuatorTriggerConfigurationEntity;

    try {
      actuatorTriggerConfiguration =
        await this._iotActuatorTriggerConfigurationService.findOne({
          where: {
            alert: {
              identifier: alert.alertConfiguration.identifier,
            },
            triggerState: alert.status,
          },
          relations: {
            owner: true,
            parent: {
              parentDevice: true,
            },
          },
        });
    } catch (err) {
      this._logger.error(err);
    }

    if (!actuatorTriggerConfiguration?.isActive) {
      this._logger.warn(
        `Conditions for actuator trigger ${actuatorTriggerConfiguration?.identifier} were met but the configuration is not active.`,
      );
      return;
    }

    if (!!actuatorTriggerConfiguration) {
      try {
        await this._iotActuatorService.postValue(
          actuatorTriggerConfiguration.parent,
          actuatorTriggerConfiguration.targetValue,
        );
      } catch (err) {
        this._logger.error(
          `Error when saving IoTActuatorTriggerConfiguration: ${err}`,
        );
      }

      try {
        const triggerAction = await this._iotActuatorTriggerService.create({
          owner: actuatorTriggerConfiguration.owner,
          configuration: actuatorTriggerConfiguration,
          alert: alert,
          previousState: alert.status,
          targetValue: actuatorTriggerConfiguration.targetValue,
        });
        await this._iotActuatorTriggerService.save(triggerAction);
      } catch (err) {
        this._logger.error(
          `Error when saving IoTActuatorTriggerAction: ${err}`,
        );
      }
    } else {
      this._logger.warn(
        `IoTAlertConfiguration was not found for alert ${data.alert}`,
      );
    }
  }
}
