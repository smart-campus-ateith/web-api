import { ConflictException, Injectable, Logger } from '@nestjs/common';
import {
  ApplicationEventSubject,
  EncapsulatedError,
  EntityService,
  ISensorLabelCreatedEvent,
  ISensorLabelRemovedEvent,
  ISensorLabelUpdatedEvent,
} from '../../../common';
import { SensorLabelEntity } from '../../entities/sensor-label.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { DeepPartial, FindOptionsWhere, Repository } from 'typeorm';
import { ApplicationEventsService } from '../../../common/services/application-events.service';
import { QueryDeepPartialEntity } from 'typeorm/query-builder/QueryPartialEntity';
import { defaultLabelAdditionalName } from '../../iot-domain.constants';

@Injectable()
export class SensorLabelService extends EntityService<SensorLabelEntity> {
  private readonly _logger = new Logger(`IoTDomain/${SensorLabelService.name}`);

  constructor(
    @InjectRepository(SensorLabelEntity)
    private readonly _sensorLabelRepository: Repository<SensorLabelEntity>,
    private readonly _applicationEventService: ApplicationEventsService,
  ) {
    super(SensorLabelEntity.name);
    this.repository = _sensorLabelRepository;
  }

  async save(options: DeepPartial<SensorLabelEntity>) {
    const result = await super.save(options);

    const event: ISensorLabelCreatedEvent = {
      subject: ApplicationEventSubject.SensorLabelCreated,
      payload: {
        label: result.identifier,
        name: result.name,
        owner: result.owner.identifier,
      },
    };

    this._applicationEventService.emit(event);
    return result;
  }

  async update(id: number, fields: QueryDeepPartialEntity<SensorLabelEntity>) {
    const result = await super.update(id, fields);

    const target = await this.findOne({
      where: {
        id,
      },
    });

    const event: ISensorLabelUpdatedEvent = {
      subject: ApplicationEventSubject.SensorLabelUpdated,
      payload: {
        label: target.identifier,
        name: target.name,
      },
    };

    this._applicationEventService.emit(event);

    return result;
  }

  async updateMany(
    criteria: FindOptionsWhere<SensorLabelEntity>,
    fields: QueryDeepPartialEntity<SensorLabelEntity>,
  ) {
    const result = await super.updateMany(criteria, fields);

    const targets = await super.findAll({
      where: criteria,
    });

    for (let i = 0; i < targets.length; i++) {
      const event: ISensorLabelUpdatedEvent = {
        subject: ApplicationEventSubject.SensorLabelUpdated,
        payload: {
          label: targets[i].identifier,
          name: targets[i].name,
        },
      };

      this._applicationEventService.emit(event);
    }

    return result;
  }

  async softRemove(criteria: FindOptionsWhere<SensorLabelEntity>) {
    const label: SensorLabelEntity = await this._sensorLabelRepository.findOne({
      where: criteria,
    });

    if (label.additionalName === defaultLabelAdditionalName) {
      const error = new ConflictException(
        'The default sensor label can not be deleted',
      );
      throw new EncapsulatedError(error, error);
    }

    const result = await super.softRemove(criteria);
    const removed = await super.findAll({
      where: criteria,
      withDeleted: true,
    });

    for (let i = 0; i < removed.length; i++) {
      const event: ISensorLabelRemovedEvent = {
        subject: ApplicationEventSubject.SensorLabelRemoved,
        payload: {
          label: removed[i].identifier,
        },
      };

      this._applicationEventService.emit(event);
    }

    return result;
  }
}
