import { Injectable, Logger } from '@nestjs/common';
import {
  ApplicationEventsService,
  ApplicationEventSubject,
  EventHandlerService,
  IoTTaskAlertCheckResultReceivedEvent,
} from '../../../common';
import { OnEvent } from '@nestjs/event-emitter';
import { IoTAlertConfigurationEntity } from '../../entities/iot-alert-configuration.entity';
import { IoTAlertService } from '../iot-alert/iot-alert.service';
import { IoTAlertConfigurationService } from './iot-alert-configuration.service';

@Injectable()
export class IotAlertConfigurationHandlerService extends EventHandlerService {
  constructor(
    private readonly _iotAlertConfigurationService: IoTAlertConfigurationService,
    private readonly _iotAlertService: IoTAlertService,
    private readonly _applicationEventsService: ApplicationEventsService,
  ) {
    super(new Logger(`IoTDomain/${IotAlertConfigurationHandlerService.name}`));
  }

  @OnEvent(ApplicationEventSubject.IoTTaskAlertCheckResultReceived)
  async onIoTTaskAlertCheckResultReceived(
    data: IoTTaskAlertCheckResultReceivedEvent['payload'],
  ) {
    this.logEvent(
      ApplicationEventSubject.IoTTaskAlertCheckResultReceived,
      data,
    );
    let alertConfiguration: IoTAlertConfigurationEntity;

    try {
      alertConfiguration = await this._iotAlertConfigurationService.findOne({
        where: {
          task: {
            identifier: data.task,
          },
        },
        relations: ['owner'],
      });
    } catch (err) {
      this._logger.error(err);
    }

    if (alertConfiguration) {
      this._iotAlertService
        .save({
          jobId: data.jobId,
          workerVersion: data.workerVersion,
          alertConfiguration: alertConfiguration,
          owner: alertConfiguration.owner,
          status: data.status,
          prevStatus: data.prevStatus,
          error: data.error,
          value: data.value,
        })
        .catch((err) => this._logger.error(err.stack ?? err));
    } else {
      this._logger.warn(
        `IoTAlerConfiguration was not found for task ${data.task}`,
      );
    }
  }
}
