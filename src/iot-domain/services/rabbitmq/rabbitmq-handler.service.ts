import { Injectable, Logger } from '@nestjs/common';
import {
  ApplicationEventSubject,
  EventHandlerService,
  IIoTDeviceCreatedEvent,
  IIoTDeviceSoftRemovedEvent,
  IIoTDeviceTokenCreatedEvent,
} from '../../../common';
import { OnEvent } from '@nestjs/event-emitter';
import { RabbitmqService } from './rabbitmq.service';

@Injectable()
export class RabbitmqHandlerService extends EventHandlerService {
  constructor(private readonly _rabbitMQService: RabbitmqService) {
    super(new Logger(RabbitmqHandlerService.name));
  }

  @OnEvent(ApplicationEventSubject.IoTDeviceTokenCreated)
  async handleTokenCreated(payload: IIoTDeviceTokenCreatedEvent['payload']) {
    this.logEvent(ApplicationEventSubject.IoTDeviceTokenCreated, payload);
    this._rabbitMQService
      .compareStates()
      .catch((err) => this._logger.error(err))
      .finally(() =>
        this._logger.log(
          `Updated token for RabbitMQ user ${payload.identifier}`,
        ),
      );
  }

  @OnEvent(ApplicationEventSubject.IoTDeviceSoftRemoved)
  async handleDeviceRemoved(payload: IIoTDeviceSoftRemovedEvent['payload']) {
    this.logEvent(ApplicationEventSubject.IoTDeviceSoftRemoved, payload);
    this._rabbitMQService
      .removeUser(payload.identifier)
      .catch((err) => this._logger.error(err?.stack ?? err))
      .finally(() =>
        this._logger.log(`Removed RabbitMQ user ${payload.identifier}`),
      );
  }
}
