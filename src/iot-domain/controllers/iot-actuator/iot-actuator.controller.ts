import {
  ClassSerializerInterceptor,
  Controller,
  Get,
  Logger,
  UseGuards,
  UseInterceptors,
  Request,
  Query,
  Param,
  Delete,
  Post,
  Body,
  Put,
  BadRequestException,
} from '@nestjs/common';
import { IotActuatorService } from '../../services/iot-actuator/iot-actuator.service';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { IsUserAuthGuard, ValidJwtAuthGuard } from '../../../auth';
import {
  BaseController,
  EncapsulatedErrorService,
  EntityIdentifierDTO,
  IResponse,
  PaginationQueryDto,
  QueryService,
} from '../../../common';
import { IoTActuatorEntity } from '../../entities/iot-actuator.entity';
import {
  IoTActuatorCreateDTO,
  IotActuatorQueryDTO,
  IoTActuatorSetValueDTO,
  IoTActuatorUpdateDTO,
} from '../../dto/iot-actuator.dto';
import { IotDeviceService } from '../../services/iot-device/iot-device.service';
import { SensorLabelService } from '../../services/sensor-label/sensor-label.service';
import { In } from 'typeorm';
import { SensorLabelEntity } from '../../entities/sensor-label.entity';
import { IotDeviceEntity } from '../../entities/iot-device.entity';

@Controller('iot-actuator')
@ApiTags('iot-actuator')
@UseGuards(ValidJwtAuthGuard, IsUserAuthGuard)
@ApiBearerAuth()
@UseInterceptors(ClassSerializerInterceptor)
export class IotActuatorController extends BaseController<IoTActuatorEntity> {
  constructor(
    protected readonly _iotActuatorService: IotActuatorService,
    protected readonly _encapsulatedErrorService: EncapsulatedErrorService,
    protected readonly _queryService: QueryService,
    private readonly _iotDeviceService: IotDeviceService,
    private readonly _sensorLablesService: SensorLabelService,
  ) {
    super(
      _iotActuatorService,
      _queryService,
      _encapsulatedErrorService,
      new Logger(`IoTDomain/${IotActuatorController.name}`),
    );
  }

  @Get()
  async get(
    @Request() req,
    @Query() pagination: PaginationQueryDto,
    @Query() query: IotActuatorQueryDTO,
  ): Promise<IResponse<IoTActuatorEntity[]>> {
    let labels: SensorLabelEntity[];

    try {
      if (Array.isArray(query.labels)) {
        labels = await this._sensorLablesService.findAll({
          where: {
            owner: {
              id: req.user.id,
            },
            identifier: Array.isArray(query.labels)
              ? In(query.labels || [])
              : query.label,
          },
        });
      } else if (query.label) {
        const label = await this._sensorLablesService.findOne({
          where: {
            owner: {
              id: req.user.id,
            },
            identifier: query.label,
          },
        });

        labels = [label];
      }
    } catch (err) {
      this.getThrowable(err);
    }

    return super.get(
      req,
      pagination,
      query,
      {
        where: {
          owner: {
            id: req.user.id,
          },
          identifier: query?.identifier,
          valueType: query?.valueType,
          parentDevice: {
            identifier: query?.parentDevice,
          },
          labels: {
            id: labels?.length > 0 ? In(labels.map(({ id }) => id)) : undefined,
          },
        },
        order: {
          createdAt: 'DESC',
        },
        relations: ['owner', 'labels', 'measurements', 'parentDevice'],
      },
      ['name', 'description'],
    );
  }

  @Get('/:identifier')
  async getOne(@Request() req, @Param() params: EntityIdentifierDTO) {
    return super.getOne(req, params, {
      where: {
        owner: {
          id: req.user.id,
        },
        identifier: params.identifier,
      },
      relations: ['owner', 'labels', 'measurements', 'parentDevice'],
    });
  }

  @Post()
  async create(
    @Request() req,
    @Body() dto: IoTActuatorCreateDTO,
  ): Promise<IResponse<IoTActuatorEntity>> {
    let device: IotDeviceEntity;
    let labels: SensorLabelEntity[];
    let actuator: IoTActuatorEntity;

    try {
      device = await this._iotDeviceService.findOneOrThrow({
        where: {
          owner: {
            id: req.user.id,
          },
          identifier: dto.parentDevice,
        },
        relations: ['owner'],
      });

      if (Array.isArray(dto.labels) && dto.labels.length > 0) {
        labels = await this._sensorLablesService.findAll({
          where: {
            owner: {
              id: req.user.id,
            },
            identifier: In(dto.labels),
          },
        });
      }
    } catch (err) {
      throw this.getThrowable(err, err);
    }

    try {
      actuator = await this._iotActuatorService.create({
        owner: req.user,
        name: dto.name,
        description: dto.description,
        valueType: dto.valueType,
        lowerLimit: dto.lowerLimit,
        upperLimit: dto.upperLimit,
        reportFormat: dto.reportFormat,
        parentDevice: device,
        labels,
      });

      actuator = await this._iotActuatorService.save(actuator);
    } catch (err) {
      throw this.getThrowable(err);
    }

    return {
      data: actuator,
    };
  }

  @Put('/:identifier')
  async update(
    @Request() req,
    @Param() params: EntityIdentifierDTO,
    @Body() dto: IoTActuatorUpdateDTO,
  ) {
    let actuator: IoTActuatorEntity;
    let device: IotDeviceEntity;
    let labels: SensorLabelEntity[];

    // resolve dependencies
    try {
      actuator = await this._iotActuatorService.findOneOrThrow({
        where: {
          owner: {
            id: req.user.id,
          },
          identifier: params.identifier,
        },
        relations: {
          owner: true,
          labels: true,
          parentDevice: {
            owner: true,
          },
        },
      });

      if (
        dto.parentDevice &&
        dto.parentDevice !== actuator.parentDevice.identifier
      ) {
        device = await this._iotDeviceService.findOneOrThrow({
          where: {
            owner: {
              id: req.user.id,
            },
            identifier: dto?.parentDevice?.identifier,
          },
        });
      }

      if (Array.isArray(dto.labels)) {
        labels = await this._sensorLablesService.findAll({
          where: {
            owner: {
              id: req.user.id,
            },
            identifier: In(dto.labels),
          },
        });
      }
    } catch (err) {
      throw this.getThrowable(err, err);
    }

    const editableFields = this.filterUpdatableFields(dto, [
      'name',
      'description',
      'lowerLimit',
      'upperLimit',
      'reportFormat',
    ]);

    // make updates
    try {
      if (device) {
        editableFields.parentDevice = device;
      }

      if (Array.isArray(labels)) {
        editableFields.labels = labels;
      }

      if (Object.keys(editableFields).length > 0) {
        actuator = await this._iotActuatorService.create({
          ...actuator,
          ...editableFields,
        });

        actuator = await this._iotActuatorService.save(actuator);
      }
    } catch (err) {
      throw this.getThrowable(err);
    }

    return {
      data: actuator,
    };
  }

  @Delete('/:identifier')
  async remove(@Request() req, @Param() param: EntityIdentifierDTO) {
    return super.remove(req, param, {
      where: {
        owner: {
          id: req.user.id,
        },
        identifier: param.identifier,
      },
    });
  }

  @Post('/:identifier/set-value')
  async updateValue(
    @Request() req,
    @Param() params: EntityIdentifierDTO,
    @Body() dto: IoTActuatorSetValueDTO,
  ) {
    let actuator;

    try {
      actuator = await this._iotActuatorService.findOne({
        where: {
          owner: {
            id: req.user.id,
          },
          identifier: params.identifier,
        },
        relations: ['parentDevice'],
      });
    } catch (err) {
      this.getThrowable(err);
    }

    if (!actuator.validateValue(dto.value)) {
      throw new BadRequestException(
        `${this._iotActuatorService.name} with identifier ${actuator.identifier} accepts only values of type ${actuator.valueType}`,
      );
    }

    const result = await this._iotActuatorService.postValue(
      actuator,
      dto.value,
    );

    return {
      data: result,
    };
  }

  @Post('/:identifier/get-value')
  async getValue(@Request() req, @Param() params: EntityIdentifierDTO) {
    let actuator;

    try {
      actuator = await this._iotActuatorService.findOneOrThrow({
        where: {
          owner: {
            id: req.user.id,
          },
          identifier: params.identifier,
        },
        relations: ['parentDevice'],
      });

      actuator = await this._iotActuatorService.create(actuator);
    } catch (err) {
      this.getThrowable(err);
    }

    const result = await this._iotActuatorService.getValue(actuator);

    return {
      data: result,
    };
  }
}
