import {
  Body,
  ClassSerializerInterceptor,
  Controller,
  Delete,
  Get,
  Logger,
  NotFoundException,
  Param,
  Post,
  Put,
  Query,
  Request,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { IoTAlertService } from '../../services/iot-alert/iot-alert.service';
import {
  BaseController,
  EncapsulatedErrorService,
  EntityIdentifierDTO,
  IResponse,
  IsDevModeGuard,
  PaginationQueryDto,
  QueryService,
} from '../../../common';
import { IoTAlertEntity } from '../../entities/iot-alert.entity';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { IsUserAuthGuard, ValidJwtAuthGuard } from '../../../auth';
import {
  IotAlertCreateDTO,
  IoTAlertQueryDTO,
  IoTAlertUpdateDTO,
} from '../../dto/iot-alert.dto';
import { IoTAlertConfigurationService } from '../../services/iot-alert-configuration/iot-alert-configuration.service';
import { ConfigService } from '@nestjs/config';
import { IConfiguration } from '../../../config/config.inteface';

@Controller('iot-alert')
@ApiTags('iot-alert')
@UseGuards(ValidJwtAuthGuard, IsUserAuthGuard)
@ApiBearerAuth()
@UseInterceptors(ClassSerializerInterceptor)
export class IoTAlertController extends BaseController<IoTAlertEntity> {
  constructor(
    protected readonly _iotAlertService: IoTAlertService,
    protected readonly _queryService: QueryService,
    protected readonly _encapsulatedErrorService: EncapsulatedErrorService,
    private readonly _alertConfigurationService: IoTAlertConfigurationService,
    private readonly _configService: ConfigService,
  ) {
    super(
      _iotAlertService,
      _queryService,
      _encapsulatedErrorService,
      new Logger(IoTAlertController.name),
    );

    const env: IConfiguration['app']['environment'] =
      this._configService.get('app.environment');
    if (env === 'dev') {
      setImmediate(() => {
        this._logger.warn(
          `Running in dev mode. ${IoTAlertController.name} allows debugging endpoints`,
        );
      });
    }
  }

  @Get()
  async get(
    @Request() req,
    @Query() paginationQuery: PaginationQueryDto,
    @Query() filtersQuery: IoTAlertQueryDTO,
  ): Promise<IResponse<IoTAlertEntity[]>> {
    return super.get(req, paginationQuery, filtersQuery, {
      where: {
        owner: {
          id: req.user.id,
        },
        identifier: filtersQuery?.identifier,
        alertConfiguration: {
          identifier: filtersQuery?.configuration,
        },
        status: filtersQuery?.status,
        acknowledged: filtersQuery?.acknowledged,
      },
      order: {
        createdAt: 'ASC',
      },
      relations: ['alertConfiguration'],
    });
  }

  @Get(':identifier')
  async getOne(@Request() req, @Param() params: EntityIdentifierDTO) {
    return super.getOne(req, params, {
      where: {
        owner: {
          id: req.user.id,
        },
        identifier: params.identifier,
      },
      relations: ['alertConfiguration'],
    });
  }

  @Post()
  @UseGuards(IsDevModeGuard)
  async create(
    @Request() req,
    @Body() dto: IotAlertCreateDTO,
  ): Promise<IResponse<IoTAlertEntity>> {
    let alertConfiguration;
    let alertData;
    let alert;

    try {
      alertConfiguration = await this._alertConfigurationService.findOne({
        where: {
          owner: {
            id: req.user.id,
          },
          identifier: dto.alertConfiguration,
        },
      });
    } catch (err) {
      throw this.getThrowable(err);
    }

    if (!alertConfiguration) {
      const error = new NotFoundException(
        `IoTAlertConfiguration ${dto.alertConfiguration} was not found`,
      );
      throw this.getThrowable(error, error);
    }

    try {
      alertData = await this._iotAlertService.create({
        status: dto.status,
        owner: req.user,
        alertConfiguration: alertConfiguration,
      });

      alert = await this._iotAlertService.save(alertData);
    } catch (err) {
      throw this.getThrowable(err);
    }

    return {
      data: alert,
    };
  }

  @Delete(':identifier')
  async remove(@Request() req, @Param() params: EntityIdentifierDTO) {
    return super.remove(req, params, {
      where: {
        owner: {
          id: req.user.id,
        },
        identifier: params.identifier,
      },
    });
  }

  @Put(':identifier')
  async update(
    @Request() req,
    @Param() params: EntityIdentifierDTO,
    @Body() dto: IoTAlertUpdateDTO,
  ) {
    let alert: IoTAlertEntity;
    let updated: IoTAlertEntity;

    try {
      alert = await this._iotAlertService.findOne({
        where: {
          owner: {
            id: req.user.id,
          },
          identifier: params.identifier,
        },
        order: {
          createdAt: 'DESC',
        },
        relations: ['owner', 'alertConfiguration'],
      });
    } catch (err) {
      throw this.getThrowable(err);
    }

    if (!alert) {
      const error = new NotFoundException(
        `${this._iotAlertService.name} with identifier ${params.identifier} was not found`,
      );
      throw this.getThrowable(error, error);
    }

    if (dto.acknowledged === true) {
      updated = await this._iotAlertService.save({
        ...alert,
        acknowledged: true,
      });
    }

    return {
      data: updated,
    };
  }
}
