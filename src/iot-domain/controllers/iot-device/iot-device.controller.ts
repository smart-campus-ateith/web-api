import {
  Controller,
  Delete,
  Get,
  Post,
  Put,
  UseGuards,
  Request,
  Body,
  ClassSerializerInterceptor,
  UseInterceptors,
  Logger,
  NotFoundException,
  Param,
  InternalServerErrorException,
  Query,
} from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { IsUserAuthGuard, ValidJwtAuthGuard } from '../../../auth';
import { IotDeviceService } from '../../services/iot-device/iot-device.service';
import {
  createIoTDeviceInputDTO,
  GenerateTokenDTO,
  InvalidateTokenDTO,
  IoTDeviceQueryDTO,
  UpdateIoTDeviceInputDTO,
} from '../../dto/iot-device.dto';
import { IotDeviceEntity } from '../../entities/iot-device.entity';
import {
  EncapsulatedError,
  EncapsulatedErrorService,
  EntityIdentifierDTO,
  PaginationQueryDto,
  IResponse,
  QueryService,
} from '../../../common';
import { IotDeviceAccessTokenService } from '../../services/iot-device-access-token/iot-device-access-token.service';

@Controller('iot-device')
@ApiTags('iot-device')
@UseGuards(ValidJwtAuthGuard, IsUserAuthGuard)
@ApiBearerAuth()
@UseInterceptors(ClassSerializerInterceptor)
export class IotDeviceController {
  private readonly _logger = new Logger(
    `IoTDomain/${IotDeviceController.name}`,
  );

  constructor(
    private readonly _iotDeviceService: IotDeviceService,
    private readonly _encapsulatedErrorService: EncapsulatedErrorService,
    private readonly _iotDeviceAccessTokenService: IotDeviceAccessTokenService,
    private readonly _queryService: QueryService,
  ) {}

  @Get('/')
  async getUserIotDevices(
    @Request() req,
    @Query() filtersQuery: IoTDeviceQueryDTO,
    @Query() pagination: PaginationQueryDto,
  ): Promise<IResponse<IotDeviceEntity[]>> {
    const { take, skip, page, perPage } =
      this._queryService.resolvePagination(pagination);

    const { searchFactors } = this._queryService.resolveSearchQuery(
      filtersQuery,
      ['name', 'description'],
    );

    const whereClause = {
      owner: {
        id: req.user.id,
        deletedDate: null,
      },
      identifier: filtersQuery?.identifier,
    };

    const whereClauseWithSearch =
      this._iotDeviceService.formatFindOptionsWithSearch(
        whereClause,
        searchFactors,
      );
    const [devices, devicesCount] = await this._iotDeviceService.findAndCount({
      take,
      skip,
      where: whereClauseWithSearch,
      relations: ['sensors', 'actuators'],
      order: {
        createdAt: 'DESC',
      },
    });

    return {
      data: devices,
      total: devicesCount,
      page,
      perPage,
    };
  }

  @Get('/:identifier')
  async getIoTDevice(
    @Request() req,
    @Param() params: EntityIdentifierDTO,
  ): Promise<IResponse<IotDeviceEntity>> {
    const device = await this._iotDeviceService.findOne({
      where: {
        identifier: params.identifier,
        deletedDate: null,
        owner: {
          id: req.user.id,
        },
      },
      relations: ['sensors', 'actuators'],
    });

    if (!device) {
      const notFoundError = new NotFoundException(
        `IoT device with identifier ${params.identifier} was not found`,
      );
      throw this._encapsulatedErrorService.formatError(
        this._logger,
        new EncapsulatedError(notFoundError, notFoundError),
      );
    }

    return {
      data: device,
    };
  }

  @Post()
  async createIotDevice(
    @Request() req,
    @Body() dto: createIoTDeviceInputDTO,
  ): Promise<IResponse<IotDeviceEntity>> {
    const device = await this._iotDeviceService.save({
      ...dto,
      owner: req.user,
      sensors: [],
    });

    return {
      data: device,
    };
  }

  @Put('/:identifier')
  async updateIotDevice(
    @Request() req,
    @Param() params: EntityIdentifierDTO,
    @Body() dto: UpdateIoTDeviceInputDTO,
  ) {
    const device = await this._iotDeviceService.findOne({
      where: {
        owner: {
          id: req.user.id,
        },
        identifier: params.identifier,
        deletedDate: null,
      },
    });

    if (!device) {
      const notFoundError = new NotFoundException(
        `IoT device with identifier ${params.identifier} was not found`,
      );
      throw this._encapsulatedErrorService.formatError(
        this._logger,
        new EncapsulatedError(notFoundError, notFoundError),
      );
    }

    const updateParams: Partial<IotDeviceEntity> = {};
    if (dto.hasOwnProperty('name')) {
      updateParams.name = dto.name;
    }

    if (dto.hasOwnProperty('description')) {
      updateParams.description = dto.description;
    }

    const updateResult = await this._iotDeviceService.update(
      device.id,
      updateParams,
    );

    if (updateResult.affected <= 0) {
      const updateError = new InternalServerErrorException(
        `Could not update device with identifier ${params.identifier}`,
      );
      throw this._encapsulatedErrorService.formatError(
        this._logger,
        new EncapsulatedError(updateError, updateError),
      );
    }

    const updatedDevice = await this._iotDeviceService.findOne({
      where: {
        owner: {
          id: req.user.id,
        },
        identifier: params.identifier,
        deletedDate: null,
      },
    });

    return {
      data: updatedDevice,
    };
  }

  @Delete('/:identifier')
  async deleteIotDevice(@Request() req, @Param() params: EntityIdentifierDTO) {
    const device = await this._iotDeviceService.findOne({
      where: {
        owner: {
          id: req.user.id,
        },
        identifier: params.identifier,
        deletedDate: null,
      },
    });

    if (!device) {
      const notFoundError = new NotFoundException(
        `IoT device with identifier ${params.identifier} was not found`,
      );
      throw this._encapsulatedErrorService.formatError(
        this._logger,
        new EncapsulatedError(notFoundError, notFoundError),
      );
    }

    await this._iotDeviceService.softRemove(device.id);
    await this._iotDeviceAccessTokenService.softRemove({
      device: {
        id: device.id,
      },
    });

    const removedDevice = await this._iotDeviceService.findOne({
      where: {
        owner: {
          id: req.user.id,
        },
        identifier: params.identifier,
      },
      withDeleted: true,
    });

    return {
      data: removedDevice,
    };
  }

  @Post('/:identifier/generate-token')
  async generateToken(
    @Request() req,
    @Param() params: EntityIdentifierDTO,
    @Body() dto: GenerateTokenDTO,
  ) {
    const device = await this._iotDeviceService.findOne({
      where: {
        owner: {
          id: req.user.id,
        },
        identifier: params.identifier,
      },
      relations: ['owner'],
    });

    if (!device) {
      const notFoundError = new NotFoundException(
        `IoTDevice with identifier ${params.identifier} was not found`,
      );
      throw this._encapsulatedErrorService.formatError(
        this._logger,
        new EncapsulatedError(notFoundError, notFoundError),
      );
    }

    const token = this._iotDeviceAccessTokenService.generate();
    try {
      const accessToken =
        await this._iotDeviceAccessTokenService.attachToDevice(
          device,
          token,
          dto.description,
        );
      return {
        data: accessToken,
      };
    } catch (err) {
      const internalServerError = new InternalServerErrorException();
      const encapsulatedError = new EncapsulatedError(err, internalServerError);
      throw this._encapsulatedErrorService.formatError(
        this._logger,
        encapsulatedError,
      );
    }
  }

  @Post('/:identifier/invalidate-token')
  async invalidateToken(
    @Request() req,
    @Param() params: EntityIdentifierDTO,
    @Body() dto: InvalidateTokenDTO,
  ) {
    const device = await this._iotDeviceService.findOne({
      where: {
        owner: {
          id: req.user.id,
        },
        identifier: params.identifier,
      },
    });

    if (!device) {
      const notFoundError = new NotFoundException(
        `IoTDevice with identifier ${params.identifier} was not found`,
      );
      throw this._encapsulatedErrorService.formatError(
        this._logger,
        new EncapsulatedError(notFoundError, notFoundError),
      );
    }

    const token = await this._iotDeviceAccessTokenService.findOne({
      where: {
        identifier: dto.identifier,
        device: {
          id: device.id,
        },
      },
    });

    if (!token) {
      const notFoundError = new NotFoundException(
        `IoTDeviceAccessToken with identifier ${params.identifier} was not found`,
      );
      throw this._encapsulatedErrorService.formatError(
        this._logger,
        new EncapsulatedError(notFoundError, notFoundError),
      );
    }

    await this._iotDeviceAccessTokenService.invalidateToken(token.id);

    return {
      data: 'ok',
    };
  }
}
