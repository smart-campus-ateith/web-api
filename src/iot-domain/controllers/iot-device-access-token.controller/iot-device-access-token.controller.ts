import {
  ClassSerializerInterceptor,
  Controller,
  Get,
  UseGuards,
  UseInterceptors,
  Request,
  Query,
  Logger,
  InternalServerErrorException,
  NotFoundException,
  Delete,
  Param,
} from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { IsUserAuthGuard, ValidJwtAuthGuard } from '../../../auth';
import { IotDeviceAccessTokenService } from '../../services/iot-device-access-token/iot-device-access-token.service';
import { IotDeviceAccessTokenQueryDTO } from '../../dto/iot-device-access-token.dto';
import {
  EncapsulatedError,
  EncapsulatedErrorService,
  EntityIdentifierDTO,
} from '../../../common';
import { IotDeviceService } from '../../services/iot-device/iot-device.service';

@Controller('iot-device-access-token')
@ApiTags('iot-device-access-token')
@UseGuards(ValidJwtAuthGuard, IsUserAuthGuard)
@ApiBearerAuth()
@UseInterceptors(ClassSerializerInterceptor)
export class IotDeviceAccessTokenController {
  private readonly _logger = new Logger(
    `IoTDomain/${IotDeviceAccessTokenController.name}`,
  );

  constructor(
    private readonly _accessTokenService: IotDeviceAccessTokenService,
    private readonly _iotDeviceService: IotDeviceService,
    private readonly _encapsulatedErrorService: EncapsulatedErrorService,
  ) {}

  @Get()
  async getTokens(
    @Request() req,
    @Query() query: IotDeviceAccessTokenQueryDTO,
  ) {
    let device;
    let tokens;
    let tokensCount: number;

    if (query?.device) {
      try {
        device = await this._iotDeviceService.findOne({
          where: {
            identifier: query.device,
            owner: {
              id: req.user.id,
            },
          },
        });
      } catch (err) {
        throw this._encapsulatedErrorService.formatError(
          this._logger,
          new EncapsulatedError(err, new InternalServerErrorException()),
        );
      }
    }

    if (!device) {
      const error = new NotFoundException(
        `IoTDevice with identifier ${query.device} was not found`,
      );
      throw this._encapsulatedErrorService.formatError(
        this._logger,
        new EncapsulatedError(error, error),
      );
    }

    try {
      [tokens, tokensCount] = await this._accessTokenService.findAndCount({
        where: {
          device: {
            id: device.id,
          },
        },
        order: {
          createdAt: 'DESC',
        },
        relations: ['device'],
      });
    } catch (err) {
      throw this._encapsulatedErrorService.formatError(
        this._logger,
        new EncapsulatedError(err, new InternalServerErrorException()),
      );
    }

    return {
      data: tokens,
      total: tokensCount,
      page: 1,
      perPage: tokens.length,
    };
  }

  @Delete('/:identifier')
  async deleteToken(@Request() req, @Param() params: EntityIdentifierDTO) {
    let token;

    try {
      token = await this._accessTokenService.findOne({
        where: {
          identifier: params.identifier,
          owner: {
            id: req.user.id,
          },
        },
      });
    } catch (err) {
      throw this._encapsulatedErrorService.formatError(
        this._logger,
        new EncapsulatedError(err, new InternalServerErrorException()),
      );
    }

    if (!token) {
      const err = new NotFoundException(
        `IoTDeviceAccessToken ${params.identifier} was not found`,
      );
      throw this._encapsulatedErrorService.formatError(
        this._logger,
        new EncapsulatedError(err, err),
      );
    }

    try {
      await this._accessTokenService.softRemove({
        id: token.id,
      });
    } catch (err) {
      throw this._encapsulatedErrorService.formatError(
        this._logger,
        new EncapsulatedError(err, new InternalServerErrorException()),
      );
    }

    return {
      data: {
        status: 'ok',
      },
    };
  }
}
