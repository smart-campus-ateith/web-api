import { Logger, Module } from '@nestjs/common';
import { IotDeviceService } from './services/iot-device/iot-device.service';
import { SensorService } from './services/sensor/sensor.service';
import { CommonModule } from '../common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { IotDeviceEntity } from './entities';
import { SensorEntity } from './entities';
import { UsersModule } from '../users/users.module';
import { AuthModule } from '../auth/auth.module';
import { SensorController } from './controllers/sensor/sensor.controller';
import { IotDeviceController } from './controllers/iot-device/iot-device.controller';
import { IotDeviceAccessTokenEntity } from './entities';
import { IotDeviceAccessTokenService } from './services/iot-device-access-token/iot-device-access-token.service';
import { SensorMeasurementEntity } from './entities';
import { SensorMeasurementController } from './controllers/sensor-measurement/sensor-measurement.controller';
import { SensorMeasurementService } from './services/sensor-measeurement/sensor-measurement.service';
import { IotDeviceAccessTokenController } from './controllers/iot-device-access-token.controller/iot-device-access-token.controller';
import { SensorLabelEntity } from './entities';
import { SensorLabelController } from './controllers/sensor-label/sensor-label.controller';
import { SensorLabelService } from './services/sensor-label/sensor-label.service';
import { MeasurementUnitEntity } from './entities';
import { MeasurementUnitService } from './services/measurement-unit/measurement-unit.service';
import { MeasurementUnitController } from './controllers/measurement-unit/measurement-unit.controller';
import { commonMeasurementUnitSeed } from './entities/measurement-unit.seed';
import { UsersService } from '../users/services/users.service';
import { IoTActuatorEntity } from './entities';
import { IotActuatorService } from './services/iot-actuator/iot-actuator.service';
import { IotActuatorController } from './controllers/iot-actuator/iot-actuator.controller';
import { TimeSeriesModule } from '../time-series/time-series.module';
import { ConfigService } from '@nestjs/config';
import { IConfiguration } from '../config/config.inteface';
import { IoTAlertController } from './controllers/iot-alert/iot-alert.controller';
import { IoTAlertService } from './services/iot-alert/iot-alert.service';
import { BullModule } from '@nestjs/bullmq';
import { IoTAlertConfigurationController } from './controllers/iot-alert-configuration/iot-alert-configuration.controller';
import { IoTAlertConfigurationService } from './services/iot-alert-configuration/iot-alert-configuration.service';
import { IoTAlertConfigurationEntity } from './entities';
import { IotTasksModule } from '../iot-tasks/iot-tasks.module';
import { IoTAlertEntity } from './entities';
import { IotAlertConfigurationHandlerService } from './services/iot-alert-configuration/iot-alert-configuration.handler.service';
import { IotActuatorTriggerActionEntity } from './entities';
import { IotActuatorTriggerConfigurationEntity } from './entities';
import { IotActuatorTriggerConfigurationHandlerService } from './services/iot-actuator-trigger-configuration/iot-actuator-trigger-configuration.handler.service';
import { IotActuatorTriggerConfigurationService } from './services/iot-actuator-trigger-configuration/iot-actuator-trigger-configuration.service';
import { IotActuatorTriggerActionService } from './services/iot-actuator-trigger/iot-actuator-trigger.service';
import { IotActuatorTriggerConfigurationController } from './controllers/iot-actuator-trigger-configuration/iot-actuator-trigger-configuration.controller';
import { IotActuatorTriggerActionController } from './controllers/iot-actuator-trigger/iot-actuator-trigger.controller';
import { StatisticsService } from './services/statistics/statistics.service';
import { SensorLabelEventHandlerService } from './services/sensor-label/sensor-label.event-handler.service';
import { HttpModule } from '@nestjs/axios';
import { RabbitmqService } from './services/rabbitmq/rabbitmq.service';
import { RabbitmqHandlerService } from './services/rabbitmq/rabbitmq-handler.service';
import { RabbitmqPublisherService } from './services/rabbitmq/rabbitmq-publisher.service';

@Module({
  imports: [
    CommonModule,
    UsersModule,
    AuthModule,
    HttpModule,
    TypeOrmModule.forFeature([
      SensorEntity,
      IotDeviceEntity,
      IotDeviceAccessTokenEntity,
      SensorMeasurementEntity,
      SensorLabelEntity,
      MeasurementUnitEntity,
      IoTActuatorEntity,
      IoTAlertConfigurationEntity,
      IoTAlertEntity,
      IotActuatorTriggerActionEntity,
      IotActuatorTriggerConfigurationEntity,
    ]),
    TimeSeriesModule.registerAsync({
      useFactory: (configService: ConfigService) => {
        const influxDbOptions: IConfiguration['influx'] =
          configService.get('influx');
        return {
          url: influxDbOptions.url,
          org: influxDbOptions.org,
          bucket: influxDbOptions.bucket,
          token: influxDbOptions.token,
        };
      },
      inject: [ConfigService],
    }),
    BullModule.registerQueue({
      name: 'my-queue',
    }),
    BullModule.registerFlowProducerAsync({
      name: 'my-flow-producer',
    }),
    IotTasksModule,
  ],
  providers: [
    IotDeviceService,
    SensorService,
    IotDeviceAccessTokenService,
    SensorMeasurementService,
    SensorLabelService,
    MeasurementUnitService,
    IotActuatorService,
    IoTAlertService,
    IoTAlertConfigurationService,
    IotAlertConfigurationHandlerService,
    IotActuatorTriggerConfigurationHandlerService,
    IotActuatorTriggerConfigurationService,
    IotActuatorTriggerActionService,
    StatisticsService,
    SensorLabelEventHandlerService,
    RabbitmqService,
    RabbitmqHandlerService,
    RabbitmqPublisherService,
  ],
  exports: [IoTAlertService, MeasurementUnitService, StatisticsService],
  controllers: [
    IotDeviceController,
    SensorController,
    SensorMeasurementController,
    IotDeviceAccessTokenController,
    SensorLabelController,
    MeasurementUnitController,
    IotActuatorController,
    IoTAlertController,
    IoTAlertConfigurationController,
    IotActuatorTriggerConfigurationController,
    IotActuatorTriggerActionController,
  ],
})
export class IotDomainModule {
  private readonly _logger = new Logger(`IoTDomain/${IotDomainModule.name}`);

  constructor(
    private readonly _measurementUnitService: MeasurementUnitService,
    private readonly _userService: UsersService,
  ) {
    setImmediate(() => {
      this.seedMeasurementUnit().catch((err) =>
        this._logger.error(err?.stack ?? err),
      );
    });
  }

  async seedMeasurementUnit() {
    try {
      const units = await this._measurementUnitService.findAll();
      if (units.length === 0) {
        const publicUser = await this._userService.findOne({
          where: {
            username: 'public',
            person: null,
          },
        });

        if (publicUser) {
          for (let i = 0; i < commonMeasurementUnitSeed.length; i++) {
            const seed = {
              ...commonMeasurementUnitSeed[i],
              owner: publicUser,
            };
            await this._measurementUnitService.save(seed);
          }

          this._logger.log(`MeasurementUnitEntity seed completed`);
        } else {
          this._logger.error('Public user is not available');
        }
      }
    } catch (err) {
      this._logger.error(err?.stack ?? err);
    }
  }
}
