import { OwnedThing } from '../../users/entities/owned-thing.entity';
import { IoTAlertConfigurationEntity } from './iot-alert-configuration.entity';
import { Column, DeepPartial, Entity, ManyToOne } from 'typeorm';

@Entity({
  name: 'IoTDomain_IoTAlertEntity',
})
export class IoTAlertEntity extends OwnedThing {
  constructor(params: DeepPartial<IoTAlertEntity>) {
    super(params);

    if (params) {
      this.alertConfiguration = new IoTAlertConfigurationEntity(
        params.alertConfiguration,
      );
      this.status = params.status;
      this.jobId = params.jobId;
      this.workerVersion = params.workerVersion;
      this.acknowledged = params.acknowledged;
      this.value = params.value;
    }
  }

  @ManyToOne(() => IoTAlertConfigurationEntity)
  alertConfiguration?: IoTAlertConfigurationEntity;

  @Column({
    enum: ['normal', 'warning', 'critical', 'error'],
  })
  status?: 'normal' | 'warning' | 'critical' | 'error';

  @Column({
    enum: ['normal', 'warning', 'critical', 'error'],
    nullable: true,
  })
  prevStatus?: 'normal' | 'warning' | 'critical' | 'error';

  @Column({
    default: false,
  })
  acknowledged: boolean;

  @Column({
    nullable: true,
  })
  jobId?: string;

  @Column({
    nullable: true,
  })
  workerVersion?: string;

  @Column({
    nullable: true,
  })
  error?: string;

  @Column({
    nullable: true,
  })
  value?: number;
}
