import {
  Column,
  Entity,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
} from 'typeorm';
import { IotDeviceEntity } from './iot-device.entity';
import { OwnedThing } from '../../users/entities/owned-thing.entity';
import { SensorMeasurementEntity } from './sensor-measurement.entity';
import { SensorLabelEntity } from './sensor-label.entity';
import { MeasurementUnitEntity } from './measurement-unit.entity';

/**
 *
 * @class Sensor
 * @description Declares a sensor device that produces a value
 *
 * @property {string} name The name of the sensor
 * @property {string} description A brief description of the sensor
 * @property {string} valueType The format of the value that is being posted by the sensor
 * @property {string} measurementUnit The unit in which sensor measurements are collected
 * @property {IotDeviceEntity} parentDevice The iot device that the sensor is attached on
 *
 */
@Entity({
  name: 'IoTDomain_Sensor',
})
export class SensorEntity extends OwnedThing {
  get activeMeasurement() {
    return this.measurements.find((m) => m.isActive);
  }

  @Column({
    nullable: false,
  })
  name: string;

  @Column({
    nullable: false,
    default: 'float',
  })
  valueType: 'int' | 'float' | 'boolean' | 'string';

  @ManyToOne(() => MeasurementUnitEntity)
  @JoinColumn()
  measurementUnit: MeasurementUnitEntity;

  @ManyToOne(
    () => IotDeviceEntity,
    (hardwarePlatform) => hardwarePlatform.sensors,
  )
  parentDevice: IotDeviceEntity;

  @OneToMany(() => SensorMeasurementEntity, (measurement) => measurement.sensor)
  measurements: SensorMeasurementEntity[];

  @ManyToMany(() => SensorLabelEntity)
  @JoinTable({
    name: 'IoTDomain_Sensor_SensorLabel',
  })
  labels: SensorLabelEntity[];
}
