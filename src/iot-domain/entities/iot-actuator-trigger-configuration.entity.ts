import { OwnedThing } from '../../users/entities/owned-thing.entity';
import { Column, DeepPartial, Entity, ManyToOne } from 'typeorm';
import { EntityValidationResult, IoTValue } from '../../common';
import { IoTActuatorEntity } from './iot-actuator.entity';
import { IoTAlertConfigurationEntity } from './iot-alert-configuration.entity';

@Entity({
  name: 'IoTDomain_IotActuatorTriggerConfigurationEntity',
})
export class IotActuatorTriggerConfigurationEntity extends OwnedThing {
  constructor(params?: DeepPartial<IotActuatorTriggerConfigurationEntity>) {
    super(params);

    if (params) {
      this.name = params.name;
      this.parent = new IoTActuatorEntity(params.parent);
      this.alert = new IoTAlertConfigurationEntity(params.alert);
      this.triggerState = params.triggerState;
      this.targetValue = params.targetValue;
      this.isActive = params.isActive;
    }
  }

  @Column({
    default: false,
  })
  name?: string;

  @Column({
    enum: ['normal', 'warning', 'critical', 'error'],
  })
  triggerState?: 'normal' | 'warning' | 'critical' | 'error';

  @Column({
    default: false,
  })
  targetValue: IoTValue;

  @ManyToOne(() => IoTActuatorEntity, {
    onDelete: 'CASCADE',
  })
  parent?: IoTActuatorEntity;

  @ManyToOne(() => IoTAlertConfigurationEntity, {
    onDelete: 'CASCADE',
  })
  alert?: IoTAlertConfigurationEntity;

  @Column({
    default: true,
  })
  isActive: boolean;

  validate(): EntityValidationResult {
    const validation = super.validate();

    return {
      validationErrors: validation.validationErrors,
      isValid: validation.validationErrors.length === 0,
    };
  }
}

// Create a function that validates an email
