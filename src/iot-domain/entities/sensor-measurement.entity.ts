import { Column, DeepPartial, Entity, ManyToOne } from 'typeorm';
import { OwnedThing } from '../../users/entities/owned-thing.entity';
import { SensorEntity } from './sensor.entity';
import { IoTActuatorEntity } from './iot-actuator.entity';
import { SensorMeasurement } from '../dto/sensor-measurement.dto';
import { EntityValidationResult } from '../../common';

/**
 *
 * @class Sensor
 * @description Declares a sensor device that produces a value
 *
 * @property {string} name The name of the sensor
 *
 */
@Entity({
  name: 'IoTDomain_SensorMeasurement',
})
export class SensorMeasurementEntity extends OwnedThing {
  constructor(props?: DeepPartial<SensorMeasurementEntity>) {
    super(props);

    if (props) {
      this.name = props.name;
      this.additionalType = props.additionalType;
      this.sensor = props.sensor as SensorEntity;
      this.actuator = props.actuator as IoTActuatorEntity;
      this.isActive = props.isActive;
      this.isProduction = props.isProduction;
    }
  }

  private static _AdditionalType = {
    SensorMeasurement: 'SensorMeasurement',
    ActuatorMeasurement: 'ActuatorMeasurement',
  };

  static get AdditionalType() {
    return this._AdditionalType;
  }

  @Column({
    nullable: false,
  })
  name: string;

  @Column({
    nullable: true,
    enum: [
      SensorMeasurementEntity.AdditionalType.SensorMeasurement,
      SensorMeasurementEntity.AdditionalType.ActuatorMeasurement,
    ],
  })
  additionalType?: string;

  @ManyToOne(() => SensorEntity, (sensor) => sensor.measurements, {
    nullable: true,
  })
  sensor?: SensorEntity;

  @ManyToOne(() => IoTActuatorEntity, (actuator) => actuator.measurements, {
    nullable: true,
  })
  actuator?: IoTActuatorEntity;

  @Column({
    default: false,
  })
  isActive?: boolean;

  @Column({
    default: false,
  })
  isProduction?: boolean;

  validate(): EntityValidationResult {
    const validationErrors: string[] = [];

    const hasUniqueParent =
      (this.sensor && !this.actuator) || (!this.sensor && this.actuator);
    const hasAdditionalType =
      (this.sensor &&
        this.additionalType ===
          SensorMeasurementEntity.AdditionalType.SensorMeasurement) ||
      (this.actuator &&
        this.additionalType ===
          SensorMeasurementEntity.AdditionalType.ActuatorMeasurement);

    if (!hasUniqueParent) {
      validationErrors.push(
        `${SensorMeasurementEntity.name} can only belong to either an sensor an actuator`,
      );
    }

    if (!hasAdditionalType) {
      validationErrors.push(
        `${SensorMeasurementEntity.name} must have additionalType that corresponds to it's parent Entity`,
      );
    }

    return {
      isValid: validationErrors.length === 0,
      validationErrors,
    };
  }
}
