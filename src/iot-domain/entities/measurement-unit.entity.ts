import { Column, Entity } from 'typeorm';
import { OwnedThing } from '../../users/entities/owned-thing.entity';

@Entity({
  name: 'IoTDomain_MeasurementUnit',
})
export class MeasurementUnitEntity extends OwnedThing {
  @Column({
    nullable: false,
  })
  name: string;

  @Column({
    nullable: false,
  })
  symbol: string;
}
