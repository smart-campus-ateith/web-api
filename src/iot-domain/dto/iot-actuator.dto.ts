import { ApiProperty } from '@nestjs/swagger';
import { IoTValue, ResourceSearchQueryDTO } from '../../common';
import {
  IsArray,
  IsIn,
  IsNotEmpty,
  IsNumber,
  IsOptional,
  IsString,
  IsUUID,
} from 'class-validator';

export class IotActuatorQueryDTO extends ResourceSearchQueryDTO {
  @ApiProperty({
    required: false,
  })
  @IsOptional()
  identifier?: string;

  @ApiProperty({
    required: false,
  })
  @IsOptional()
  valueType: IoTValue;

  @ApiProperty({
    required: false,
  })
  @IsOptional()
  @IsUUID()
  parentDevice?: string;

  @ApiProperty()
  @IsOptional()
  @IsArray()
  labels?: string[];

  @ApiProperty()
  @IsOptional()
  @IsUUID()
  label?: string;
}

export class IoTActuatorCreateDTO {
  @ApiProperty()
  @IsNotEmpty()
  name: string;

  @ApiProperty({
    required: false,
  })
  @IsOptional()
  description?: string;

  @ApiProperty()
  @IsString()
  @IsIn(['int', 'float', 'boolean', 'string'])
  valueType?: IoTValue;

  @ApiProperty({
    required: false,
  })
  @IsOptional()
  @IsNumber()
  lowerLimit?: number;

  @ApiProperty({
    required: false,
  })
  @IsOptional()
  @IsNumber()
  upperLimit?: number;

  @ApiProperty({
    required: false,
  })
  @IsOptional()
  @IsNotEmpty()
  @IsIn(['raw', 'json'])
  reportFormat?: 'raw' | 'json';

  @ApiProperty()
  @IsUUID()
  parentDevice: string;

  @ApiProperty({
    required: false,
  })
  @IsOptional()
  @IsArray()
  labels?: string[];
}

export class IoTActuatorUpdateDTO {
  @ApiProperty({
    required: false,
  })
  @IsOptional()
  @IsNotEmpty()
  name: string;

  @ApiProperty({
    required: false,
  })
  @IsOptional()
  description?: string;

  @ApiProperty({
    required: false,
  })
  @IsOptional()
  @IsNumber()
  lowerLimit?: number;

  @ApiProperty({
    required: false,
  })
  @IsOptional()
  @IsNumber()
  upperLimit?: number;

  @ApiProperty({
    required: false,
  })
  @IsOptional()
  @IsNotEmpty()
  @IsIn(['raw', 'json'])
  reportFormat?: 'raw' | 'json';

  @ApiProperty({
    required: false,
  })
  @IsOptional()
  parentDevice: {
    identifier: string;
  };

  @ApiProperty({
    required: false,
  })
  @IsOptional()
  @IsArray()
  labels?: string[];
}

export class IoTActuatorSetValueDTO {
  @ApiProperty()
  @IsNotEmpty()
  value: IoTValue;
}
