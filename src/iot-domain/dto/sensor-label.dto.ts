import { ApiProperty } from '@nestjs/swagger';
import { IsOptional, IsString, IsUUID, MaxLength } from 'class-validator';
import { ResourceSearchQueryDTO } from '../../common';

export class SensorLabelQueryDTO extends ResourceSearchQueryDTO {
  @ApiProperty({
    required: false,
  })
  @IsUUID()
  @IsOptional()
  identifier?: string;

  @ApiProperty({
    required: false,
  })
  @IsString()
  @IsOptional()
  name?: string;

  @ApiProperty({
    required: false,
  })
  @IsString()
  @IsOptional()
  additionalName?: string;
}

export class SensorLabelCreateDTO {
  @ApiProperty({
    example: 'room-temperature',
  })
  @IsString()
  @MaxLength(20)
  name: string;

  @ApiProperty({
    required: false,
    example: 'This is a label description',
  })
  @IsOptional()
  @MaxLength(300)
  description?: string;
}

export class SensorLabelUpdateDTO {
  @ApiProperty({
    required: false,
  })
  @IsOptional()
  @IsString()
  @MaxLength(20)
  name?: string;

  @ApiProperty({
    required: false,
  })
  @IsOptional()
  @MaxLength(300)
  description?: string;
}
