import { Inject, Injectable, Logger } from '@nestjs/common';
import { InfluxEntry, InfluxQuery } from '../influxdb';
import { InfluxDB, Point } from '@influxdata/influxdb-client';
import { IConfiguration } from '../../config/config.inteface';
import { IFLUXDB_OPTIONS_TOKEN } from '../configurable-time-series';
import { IoTValue, IoTValueType } from '../../common';
import { HttpService } from '@nestjs/axios';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class InfluxdbService {
  private readonly _logger = new Logger(`TimeSeries/${InfluxdbService.name}`);
  private _influxDbClient: InfluxDB;

  constructor(
    @Inject(IFLUXDB_OPTIONS_TOKEN)
    private influxOptions: IConfiguration['influx'],
    private readonly _httpService: HttpService,
    private readonly _configService: ConfigService,
  ) {}

  get influxDbClient() {
    if (!this._influxDbClient) {
      this._influxDbClient = new InfluxDB({
        url: this.influxOptions.url,
        token: this.influxOptions.token,
      });
    }

    return this._influxDbClient;
  }

  /**
   * Stores a single measurement to the influxdb backend
   *
   * @param measurement The measurement to be stored
   *
   * @param valueType
   */
  async store(
    measurement: InfluxEntry,
    valueType: IoTValueType = IoTValueType.FLOAT,
  ) {
    const writeApi = this.influxDbClient.getWriteApi(
      this.influxOptions.org,
      this.influxOptions.bucket,
    );

    const p = new Point(measurement.measurement);
    p.tag('target_id', measurement.targetId);
    p.tag('type', measurement.type);
    const value = measurement.fields[0]['value'];
    if (valueType === IoTValueType.BOOLEAN) {
      let parsed;

      if (typeof value === 'string' && !isNaN(parseInt(value))) {
        parsed = Boolean(parseInt(value));
      } else {
        parsed = typeof value === 'string' ? value === 'true' : Boolean(value);
      }

      p.booleanField('value', parsed);
    } else if (valueType === IoTValueType.INT) {
      const parsed = typeof value === 'string' ? parseInt(value) : value;

      p.intField('value', parsed);
    } else if (valueType === IoTValueType.STRING) {
      p.stringField('value', measurement.fields[0]['value']);
    } else {
      p.floatField('value', measurement.fields[0]['value']);
    }

    if (measurement.timestamp) {
      p.timestamp(new Date(measurement.timestamp));
    }

    writeApi.writePoint(p);

    try {
      await writeApi.flush();
      await writeApi.close();
      this._logger.debug(
        `Collected measurement point for ${measurement.measurement}`,
      );
    } catch (err) {
      this._logger.error(err.stack ?? err);
      throw err;
    }
  }

  async storeBulk(
    measurements: InfluxEntry[],
    valueType: IoTValueType = IoTValueType.FLOAT,
  ) {
    const writeApi = this.influxDbClient.getWriteApi(
      this.influxOptions.org,
      this.influxOptions.bucket,
    );

    for (const measurement of measurements) {
      const p = new Point(measurement.measurement);
      p.tag('target_id', measurement.targetId);
      p.tag('type', measurement.type);

      const value = measurement.fields[0]['value'];

      if (valueType === IoTValueType.BOOLEAN) {
        let parsed;

        if (typeof value === 'string' && !isNaN(parseInt(value))) {
          parsed = Boolean(parseInt(value));
        } else {
          parsed =
            typeof value === 'string' ? value === 'true' : Boolean(value);
        }

        p.booleanField('value', parsed);
      } else if (valueType === IoTValueType.INT) {
        const parsed = typeof value === 'string' ? parseInt(value) : value;

        p.intField('value', parsed);
      } else if (valueType === IoTValueType.STRING) {
        p.stringField('value', measurement.fields[0]['value']);
      } else {
        p.floatField(
          'value',
          parseFloat(Number(measurement.fields[0]['value']).toString()),
        );
      }

      if (measurement.timestamp) {
        p.timestamp(measurement.timestamp);
      }

      writeApi.writePoint(p);
    }

    try {
      await writeApi.flush();
      await writeApi.close();
      this._logger.debug(
        `Collected ${measurements.length} measurement points for ${measurements[0].measurement}`,
      );
    } catch (err) {
      this._logger.error(err.stack ?? err);
      throw err;
    }
  }

  async removeBulk(targetIdentifier?: string, measurement?: string) {
    const stop = new Date();
    stop.setFullYear(stop.getFullYear() + 1);
    const start = new Date('2000-01-01');

    return new Promise((resolve, reject) => {
      const query = `?org=${this.influxOptions.org}&bucket=${this.influxOptions.bucket}`;
      const subscription = this._httpService
        .post(
          this.influxOptions.url + '/api/v2/delete' + query,
          JSON.stringify({
            start: start.toISOString(),
            stop: stop.toISOString(),
          }),
          {
            headers: {
              Authorization: `Bearer ${this.influxOptions.token}`,
              'Content-Type': 'application/json',
            },
          },
        )
        .subscribe((data) => {
          subscription.unsubscribe();
          if (data.status >= 400) {
            console.log(data);
            return reject(data.statusText);
          } else {
            return resolve(data);
          }
        });
    });
  }

  query(params: InfluxQuery) {
    return new Promise((resolve, reject) => {
      const queryClient = this.influxDbClient.getQueryApi(
        this.influxOptions.org,
      );

      const fluxQuery = `from(bucket: "${this.influxOptions.bucket}")
      |> range(start: ${params.start ?? '-1h'}, stop: ${params.stop ?? 'now()'})
      |> filter(fn: (r) => r["_measurement"] == "${params.measurement}")
    `;

      const values = [];
      queryClient.queryRows(fluxQuery, {
        next: (row, tableMeta) => {
          const tableObject = tableMeta.toObject(row);
          values.push(tableObject);
        },
        error: (error) => {
          return reject(error);
        },
        complete: () => {
          return resolve(values);
        },
      });
    });
  }

  getLast(options: { measurement: string }) {
    return new Promise((resolve, reject) => {
      const queryClient = this.influxDbClient.getQueryApi(
        this.influxOptions.org,
      );

      const fluxQuery = `from(bucket: "${this.influxOptions.bucket}")
        |> range(start: 0, stop: now())
        |> filter(fn: (r) => r["_measurement"] == "${options.measurement}")
        |> last()
    `;

      let value;
      queryClient.queryRows(fluxQuery, {
        next: (row, tableMeta) => {
          value = tableMeta.toObject(row);
        },
        error: (error) => {
          return reject(error);
        },
        complete: () => {
          return resolve(value);
        },
      });
    });
  }

  formatData(data: any[] | any): any[] | any {
    if (Array.isArray(data)) {
      return data.map((value) => ({
        result: value.result,
        measurement: value._measurement,
        type: value.type,
        targetId: value.target_id,
        value:
          typeof value._value === 'boolean'
            ? value._value
              ? 1
              : 0
            : value._value,
        stop: value._stop,
        start: value._start,
        field: value._field,
        table: value.table,
        time: value._time,
      }));
    } else if (typeof data !== 'undefined') {
      return {
        result: data.result,
        measurement: data._measurement,
        type: data.type,
        targetId: data.target_id,
        data:
          typeof data._value === 'boolean'
            ? data._value
              ? 1
              : 0
            : data._value,
        stop: data._stop,
        start: data._start,
        field: data._field,
        table: data.table,
        time: data._time,
      };
    } else {
      return null;
    }
  }

  getValueType(valueType: IoTValue): IoTValueType {
    switch (valueType) {
      case 'boolean':
        return IoTValueType.BOOLEAN;
      case 'string':
        return IoTValueType.STRING;
      case 'int':
        return IoTValueType.INT;
      case 'float':
        return IoTValueType.FLOAT;
      default:
        return IoTValueType.FLOAT;
    }
  }
}
