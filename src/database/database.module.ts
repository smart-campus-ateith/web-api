import { Module } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { TypeOrmModule, TypeOrmModuleOptions } from '@nestjs/typeorm';
import { IConfiguration } from 'src/config/config.inteface';
import { EntitySubscriber } from './entity-subscriber';

@Module({
  imports: [
    TypeOrmModule.forRootAsync({
      useFactory: async (configService: ConfigService) => {
        const config = configService.get(
          'database',
        ) as IConfiguration['database'];
        const dbOptions: TypeOrmModuleOptions = {
          type: 'postgres',
          host: config.host,
          port: config.port ?? 5432,
          username: config.username ?? '',
          password: config.password ?? '',
          database: config.database ?? '',
          synchronize: false,
          autoLoadEntities: true,
          subscribers: [EntitySubscriber],
          poolSize: config.poolsize,
        };

        return dbOptions;
      },
      inject: [ConfigService],
    }),
  ],
})
export class DatabaseModule {}
