import {
  ClassSerializerInterceptor,
  Controller,
  Get,
  Req,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { IsUserAuthGuard, ValidJwtAuthGuard } from '../../auth';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { StatisticsService } from '../../iot-domain/services/statistics/statistics.service';

@Controller('client')
@UseGuards(ValidJwtAuthGuard, IsUserAuthGuard)
@ApiTags('client')
@ApiBearerAuth()
@UseGuards(ValidJwtAuthGuard, IsUserAuthGuard)
@UseInterceptors(ClassSerializerInterceptor)
export class ClientController {
  constructor(private readonly _statisticsService: StatisticsService) {}

  @Get('summary')
  async get(@Req() req) {
    const result = await this._statisticsService.getActiveUserInstances(
      req.user.id,
    );

    return {
      data: result,
    };
  }
}
