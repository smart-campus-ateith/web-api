import { OwnedThing } from '../../users/entities/owned-thing.entity';
import { Column, DeepPartial, Entity } from 'typeorm';

@Entity({
  name: 'Client_Notification',
})
export class NotificationEntity extends OwnedThing {
  constructor(params?: DeepPartial<NotificationEntity>) {
    super(params);

    if (params) {
      this.acknowledged = params.acknowledged;
      this.resourceName = params.resourceName;
      this.instanceIdentifier = params.instanceIdentifier;
    }
  }

  @Column()
  resourceName?: string;

  @Column()
  resourcePath?: string;

  @Column({
    nullable: true,
  })
  instanceIdentifier?: string;

  @Column({
    default: false,
  })
  acknowledged?: boolean;
}
