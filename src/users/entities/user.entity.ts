import { Entity, JoinColumn, OneToOne, Column } from 'typeorm';
import { Person, Thing } from '../../common';

/**
 *
 * @class User
 * @description A system's user
 *
 * @property {Person} person The personal information of the user
 * @property {username} username The user's username
 * @property {string} displayName The user's preference of how they want to be shown
 * @property {string} email The user's public email
 *
 */
@Entity({
  name: 'Users_User',
})
export class User extends Thing {
  @OneToOne(() => Person)
  @JoinColumn()
  person: Person;

  @Column()
  username: string;

  @Column()
  displayName: string;

  @Column()
  email: string;

  @Column({
    nullable: false,
    type: 'boolean',
    default: false,
  })
  isAdmin: boolean;
}
