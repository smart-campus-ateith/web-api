import { Injectable } from '@nestjs/common';
import { DeepPartial, Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from '../entities/user.entity';
import {
  ApplicationEventsService,
  ApplicationEventSubject,
  EntityService,
  IUserCreatedEvent,
} from '../../common';

@Injectable()
export class UsersService extends EntityService<User> {
  constructor(
    @InjectRepository(User)
    private usersRepository: Repository<User>,
    private _applicationEventService: ApplicationEventsService,
  ) {
    super();
    this.repository = usersRepository;
  }

  async save(options: DeepPartial<User>) {
    const action = options.identifier ? 'update' : 'create';

    const result = await super.save(options);
    if (result && action === 'create') {
      const event: IUserCreatedEvent = {
        subject: ApplicationEventSubject.UserCreated,
        payload: {
          id: result.id,
          email: result.email,
        },
      };
      this._applicationEventService.emit(event);
    }

    return {} as User;
  }
}
