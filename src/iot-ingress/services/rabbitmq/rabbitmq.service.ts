import { Injectable, Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { IotIngressService } from '../iot-ingress.service';
import { IConfiguration } from '../../../config/config.inteface';
import { HttpService } from '@nestjs/axios';
import { RabbitmqBinding } from './rabbitmq.types';
// eslint-disable-next-line @typescript-eslint/no-var-requires
const amqplib = require('amqplib');

@Injectable()
export class RabbitMQService {
  private readonly _logger = new Logger(`IoTIngress/${RabbitMQService.name}`);

  constructor(
    private readonly _configService: ConfigService,
    private readonly _iotIngressService: IotIngressService,
    private readonly _httpService: HttpService,
  ) {
    this.setup().catch((err) => console.error(err));
  }

  async setup() {
    const config = this._configService.get(
      'rabbitmq',
    ) as IConfiguration['rabbitMQ'];

    const bindings = await this.listBindings(config);
    const bindingExist = this.bindingExist(bindings, {
      routingKey: '#',
      queue: config.mqttQueue,
      source: 'amq.topic',
      vhost: config.vhost,
    });

    if (!bindingExist) {
      await this.createQueue(config);
      await this.createBinding(config, {
        source: 'amq.topic',
        routingKey: '#',
      });
    }

    await this.subscribeToQueue(config);
  }

  async listBindings(
    config: IConfiguration['rabbitMQ'],
  ): Promise<RabbitmqBinding[]> {
    return new Promise((resolve, reject) => {
      const subscription = this._httpService
        .get(
          `http://${config.username}:${config.password}@${config.host}:${
            config.httpPort
          }/api/bindings/${encodeURIComponent(config.vhost)}`,
          {
            auth: {
              username: config.username,
              password: config.password,
            },
            headers: {
              'Content-Type': 'application/json',
            },
          },
        )
        .subscribe((data) => {
          subscription.unsubscribe();
          if (data.status < 400) {
            return resolve(data.data);
          } else {
            return reject(data.data);
          }
        });
    });
  }

  bindingExist(
    bindings: RabbitmqBinding[],
    params: {
      vhost: string;
      source: string;
      queue: string;
      routingKey: string;
    },
  ) {
    return bindings.find((b) => {
      return (
        b.routing_key === params.routingKey &&
        b.vhost === params.vhost &&
        b.source === params.source &&
        b.destination === params.queue
      );
    });
  }

  async createQueue(config: IConfiguration['rabbitMQ']) {
    return new Promise((resolve, reject) => {
      const path = `http://${config.host}:${
        config.httpPort
      }/api/queues/${encodeURIComponent(config.vhost)}/${config.mqttQueue}`;
      const subscription = this._httpService
        .put(
          path,
          {
            durable: true,
          },
          {
            auth: {
              username: config.username,
              password: config.password,
            },
          },
        )
        .subscribe((response) => {
          subscription.unsubscribe();
          if (response.status < 400) {
            this._logger.log(`Created queue ${config.mqttQueue}`);
            return resolve(response.data);
          } else {
            return reject(response.data);
          }
        });
    });
  }

  async createBinding(
    config: IConfiguration['rabbitMQ'],
    params: {
      source: string;
      routingKey: string;
    },
  ) {
    return new Promise((resolve, reject) => {
      const path = `http://${config.host}:${
        config.httpPort
      }/api/bindings/${encodeURIComponent(config.vhost)}/e/${params.source}/q/${
        config.mqttQueue
      }`;
      const subscription = this._httpService
        .post(
          path,
          {
            routing_key: params.routingKey,
          },
          {
            auth: {
              username: config.username,
              password: config.password,
            },
          },
        )
        .subscribe((response) => {
          subscription.unsubscribe();
          if (response.status < 400) {
            this._logger.log(
              `Created binding for amq.topic on ${config.mqttQueue}`,
            );
            return resolve(response.data);
          } else {
            return reject(response.data);
          }
        });
    });
  }

  async subscribeToQueue(config: IConfiguration['rabbitMQ']) {
    const queue = config.mqttQueue;
    const conn = await amqplib.connect(
      `amqp://${config.username}:${config.password}@${config.host}:${config.mqttPort}`,
    );

    const ch1 = await conn.createChannel();
    await ch1.assertQueue(queue);

    // Listener
    ch1.consume(queue, (msg) => {
      try {
        if (msg !== null) {
          const params = msg.fields.routingKey.split('.');
          let timestamp: Date;
          if (msg?.properties?.timestamp) {
            timestamp = new Date(Number(msg.properties.timestamp) * 1000);
          }
          if (timestamp) {
            const payload = JSON.parse(msg.content.toString());
            this._iotIngressService
              .storeForDevice(
                {
                  sensor: params[3],
                  accessToken: '',
                  value: payload.value ?? payload,
                  deviceIdentifier: params[2],
                },
                timestamp,
              )
              .catch((err) => {
                this._logger.debug(msg);
                this._logger.error(err);
              })
              .finally(() => ch1.ack(msg));
          } else {
            this._logger.warn('MQTT message does not have timestamp');
          }
        } else {
          this._logger.warn('Received null message');
        }
      } catch (err) {
        ch1.ack(msg);
        this._logger.debug(msg);
        this._logger.debug(msg?.content?.toString());
        this._logger.error(err);
      }
    });
  }
}
