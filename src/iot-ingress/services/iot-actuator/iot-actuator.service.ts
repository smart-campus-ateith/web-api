import { Injectable } from '@nestjs/common';
import { EntityService } from '../../../common';
import { IoTActuatorEntity } from '../../entities';
import { InjectRepository } from '@nestjs/typeorm';
import { DeepPartial, FindOneOptions, Repository } from 'typeorm';

@Injectable()
export class IoTActuatorService extends EntityService<IoTActuatorEntity> {
  constructor(
    @InjectRepository(IoTActuatorEntity)
    private readonly _actuatorRepository: Repository<IoTActuatorEntity>,
  ) {
    super(IoTActuatorEntity.name);
    this.repository = _actuatorRepository;
  }

  async findActuator(options: FindOneOptions<IoTActuatorEntity>) {
    return this._actuatorRepository.findOne(options);
  }

  async create(item: DeepPartial<IoTActuatorEntity>) {
    return new IoTActuatorEntity(item);
  }
}
