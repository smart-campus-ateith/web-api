import { Thing } from '../../common';
import { Entity, OneToMany } from 'typeorm';
import { SensorEntity } from './sensor.entity';
import { IoTActuatorEntity } from './iot-actuator.entity';

@Entity({
  name: 'IoTIngress_Device',
})
export class IoTDeviceEntity extends Thing {
  @OneToMany(() => SensorEntity, (sensor) => sensor.parentDevice)
  sensors: SensorEntity[];

  @OneToMany(() => IoTActuatorEntity, (actuator) => actuator.parentDevice)
  actuators: IoTActuatorEntity[];
}
