import { Thing } from '../../common';
import { Column, Entity, OneToMany } from 'typeorm';
import { SensorMeasurementEntity } from './sensor-measurement.entity';

@Entity({
  name: 'IoTIngress_Sensor',
})
export class SensorEntity extends Thing {
  @Column({
    unique: true,
  })
  identifier: string;

  @OneToMany(() => SensorMeasurementEntity, (measurement) => measurement.sensor)
  measurements: SensorMeasurementEntity[];

  @Column({
    nullable: true,
  })
  testingMode?: boolean;

  @Column({
    nullable: true,
  })
  parentDevice?: string;

  @Column({
    nullable: true,
  })
  valueType?: 'float' | 'int' | 'string' | 'boolean';
}
