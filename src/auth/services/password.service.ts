import { Injectable } from '@nestjs/common';
import { scrypt, randomBytes } from 'crypto';
import { promisify } from 'util';

const scryptAsync = promisify(scrypt);

/**
 *
 * @class PasswordService
 * @description Provides crypto functions for hashing and verify salted hashed passwords
 *
 */
@Injectable()
export class PasswordService {
  /**
   *
   * Creates a salted hash of a password
   *
   * @param {string} password The original password as given by the user
   *
   * @return {Promise<string>} The hashed password with the salt concatenated
   *
   */
  async toHash(password: string): Promise<string> {
    const salt = randomBytes(8).toString('hex');
    const buf = (await scryptAsync(password, salt, 64)) as Buffer;

    return `${buf.toString('hex')}.${salt}`;
  }

  /**
   *
   * Compares a plain text password with the a salted has password
   *
   * @param {string} storedPassword The stored password as it is in the database
   * @param {string} suppliedPassword The plain text password
   *
   * @return {Promise<boolean>} The comparison result
   *
   */
  async compare(
    storedPassword: string,
    suppliedPassword: string,
  ): Promise<boolean> {
    const [hashedPassword, salt] = storedPassword.split('.');
    const buf = (await scryptAsync(suppliedPassword, salt, 64)) as Buffer;

    return buf.toString('hex') === hashedPassword;
  }
}
