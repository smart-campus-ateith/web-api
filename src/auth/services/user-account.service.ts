import { ConflictException, Injectable } from '@nestjs/common';
import {
  EncapsulatedError,
  EntityService,
  ErrorCodes,
  Person,
} from '../../common';
import {
  UserAccount,
  UserAccountTypeEnum,
} from '../entities/user-account.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { EntityManager, Repository } from 'typeorm';
import { AuthTokenService } from './auth-token.service';
import { AuthTokenEntity } from '../entities/auth-token.entity';
import { UsersService } from '../../users/services/users.service';
import { User } from '../../users/entities/user.entity';
import { RegisterInputDTO } from '../dto/user-auth.dto';
import { PersonService } from '../../common/services/person.service';
import { PasswordService } from './password.service';
import { UserAccountLocalService } from './user-account-local.service';
import { UserAccountLocal } from '../entities/user-account-local.entity';

@Injectable()
export class UserAccountService extends EntityService<UserAccount> {
  constructor(
    @InjectRepository(UserAccount)
    private readonly _userAccountRepository: Repository<UserAccount>,
    private readonly _authTokenService: AuthTokenService,
    private readonly _usersService: UsersService,
    private readonly _personService: PersonService,
    private readonly _passwordService: PasswordService,
    private readonly _userAccountLocalService: UserAccountLocalService,
  ) {
    super();
    this.repository = this._userAccountRepository;
  }

  createAccount(dto: RegisterInputDTO) {
    return this.createAccountInTransaction(
      this._userAccountLocalService.manager,
      dto,
    );
  }

  async createAccountInTransaction(
    manager: EntityManager,
    dto: RegisterInputDTO,
  ) {
    let result: UserAccount;
    await manager.transaction(async (entityManager) => {
      const person: Person = await this._personService.save({});

      let user: User;
      try {
        user = await entityManager.save(User, {
          person: person,
          username: dto.email,
          displayName: dto.email,
          email: dto.email,
        });
      } catch (err) {
        if (err?.code === ErrorCodes.TypeORM.uniqueError) {
          const customErr = new ConflictException(
            'There is already an account with that email',
          );
          throw new EncapsulatedError(err, customErr);
        } else {
          throw err;
        }
      }

      // create the password
      const hashedPassword = await this._passwordService.toHash(dto.password);
      let userAccount: UserAccount;

      try {
        const entity = new UserAccount({
          additionalType: UserAccountTypeEnum.Local,
          email: dto.email,
          isActive: true,
          isVerified: false,
          userId: user.id,
        });

        userAccount = await entityManager.save(UserAccount, entity);
      } catch (err) {
        if (err?.code === ErrorCodes.TypeORM.uniqueError) {
          const customErr = new ConflictException(
            'There is already an account with that email',
          );
          throw new EncapsulatedError(err, customErr);
        } else {
          throw err;
        }
      }

      if (!dto.isActive) {
        const tokenExpirationDate = new Date();
        tokenExpirationDate.setDate(tokenExpirationDate.getDate() + 1);
        const token = new AuthTokenEntity({
          additionalType: 'verification',
          owner: user,
          expirationDate: tokenExpirationDate,
        });

        await entityManager.save(AuthTokenEntity, token);
      }

      // store local account
      await entityManager.save(UserAccountLocal, {
        account: userAccount,
        password: hashedPassword,
      });

      // create the user
      result = userAccount;
    });
    return result;
  }
}
