import {
  ConflictException,
  ForbiddenException,
  Injectable,
  InternalServerErrorException,
  Logger,
} from '@nestjs/common';
import { UsersService } from '../../users/services/users.service';
import { UserAccountLocalService } from './user-account-local.service';
import {
  LoginInputDTO,
  RegisterInputDTO,
  RegisterWithTokenInputDTO,
} from '../dto/user-auth.dto';
import { PasswordService } from './password.service';
import { UserAccount } from '../entities/user-account.entity';
import { User } from '../../users/entities/user.entity';
import {
  ApplicationEventsService,
  ApplicationEventSubject,
  EncapsulatedError,
  EncapsulatedErrorService,
  IUserCreatedEvent,
} from '../../common';
import { UserAccountService } from './user-account.service';
import { UserAccountLocal } from '../entities/user-account-local.entity';
import { JwtService } from '@nestjs/jwt';
import { IJWTTokenData } from '../interfaces/auth.interface';
import { AuthTokenEntity } from '../entities/auth-token.entity';
import { AuthTokenService } from './auth-token.service';
import { FindOneOptions, MoreThan } from 'typeorm';

@Injectable()
export class AuthService {
  private readonly _logger = new Logger(AuthService.name);

  constructor(
    private readonly _usersService: UsersService,
    private readonly _userAccountLocalService: UserAccountLocalService,
    private readonly _passwordService: PasswordService,
    private readonly _userAccountService: UserAccountService,
    private readonly _jwtService: JwtService,
    private readonly _authTokenService: AuthTokenService,
    private readonly _encapsulatedErrorService: EncapsulatedErrorService,
    private readonly _applicationEventsService: ApplicationEventsService,
  ) {}

  /**
   *
   * Registers a new local user account
   *
   * @param dto
   *
   */
  async registerAccount(dto: RegisterInputDTO): Promise<UserAccount> {
    return this._userAccountService.createAccount(dto);
  }

  async verifyUserCredentials(dto: LoginInputDTO) {
    let userAccount: UserAccount;

    try {
      userAccount = await this._userAccountService.findOne({
        where: {
          email: dto.email,
          deletedDate: null,
        },
      });
    } catch (err) {
      throw new EncapsulatedError(err, new InternalServerErrorException());
    }

    if (!userAccount) {
      const defaultErr = new Error(`User ${dto.email} does not exist`);
      const presentableErr = new ForbiddenException(`Invalid credentials`);
      throw new EncapsulatedError(defaultErr, presentableErr);
    }

    let localAccount: UserAccountLocal;
    try {
      localAccount = await this._userAccountLocalService.findOne({
        where: {
          account: {
            id: userAccount.id,
          },
        },
      });
    } catch (err) {
      throw new EncapsulatedError(err, new InternalServerErrorException());
    }

    if (!localAccount) {
      const defaultErr = new Error(
        `Could not find localAccount for UserAccount ${userAccount.id}`,
      );
      const presentableErr = new InternalServerErrorException(`Invalid user`);
      throw new EncapsulatedError(defaultErr, presentableErr);
    }

    const passwordMatching = await this._passwordService.compare(
      localAccount.password,
      dto.password,
    );

    if (passwordMatching !== true) {
      const presentableErr = new ForbiddenException(`Invalid credentials`);
      throw new EncapsulatedError(null, presentableErr);
    }

    return userAccount;
  }

  /**
   *
   * Generates a token for a user account
   *
   * @param {UserAccount} account The user account for which to create a JWT token
   *
   * @returns {string} The bearer token that was created
   *
   */
  generateToken(account: UserAccount): string {
    const tokenData = {
      email: account.email,
      identifier: account.identifier,
    };
    const token = this._jwtService.sign(tokenData);

    return `Bearer ${token}`;
  }

  async introspectToken(token: string): Promise<IJWTTokenData> {
    const tokenContents = await this._jwtService.verifyAsync(token);
    const tokenData: IJWTTokenData = {
      email: tokenContents.email,
      identifier: tokenContents.identifier,
    };

    return tokenData;
  }

  async accountExist(options: { email: string }) {
    return await this._usersService.findOne({
      where: {
        email: options?.email,
      },
    });
  }

  async inviteUser(initiator: User, email: string) {
    let user: User;
    let token: AuthTokenEntity;

    try {
      const userQuery: FindOneOptions<User> = {
        where: {
          email,
        },
      };

      const tokenQuery: FindOneOptions<AuthTokenEntity> = {
        where: {
          description: email,
          active: true,
          expirationDate: MoreThan(new Date()),
          usedAt: null,
        },
      };

      [user, token] = await Promise.all([
        this._usersService.findOne(userQuery),
        this._authTokenService.findOne(tokenQuery),
      ]);
    } catch (err) {
      throw this._encapsulatedErrorService.formatError(this._logger, err);
    }

    if (user) {
      throw new ConflictException(`User ${email} already exist`);
    }

    if (token) {
      throw new ConflictException(`Token already exist for ${email}`);
    }

    const expirationDate = new Date();
    expirationDate.setDate(new Date().getDate() + 7);

    let storedToken: AuthTokenEntity;
    try {
      storedToken = await this._authTokenService.save({
        owner: initiator,
        expirationDate: expirationDate,
        additionalType: 'registration',
        description: email,
        active: true,
      });
    } catch (err) {
      throw this._encapsulatedErrorService.formatError(this._logger, err);
    }

    return {
      token: storedToken,
    };
  }

  async registerWithToken(dto: RegisterWithTokenInputDTO) {
    let token: AuthTokenEntity;
    let user: UserAccount;

    await this._authTokenService.manager.transaction(async (entityManager) => {
      try {
        token = await entityManager.findOne(AuthTokenEntity, {
          where: {
            additionalType: 'registration',
            token: dto.token,
            description: dto.email,
          },
        });
      } catch (err) {
        throw this._encapsulatedErrorService.formatError(this._logger, err);
      }

      if (!token) {
        const error = new ConflictException('Registration token is invalid');
        throw this._encapsulatedErrorService.formatError(
          this._logger,
          new EncapsulatedError(error, error),
        );
      }

      const tokenValidation = token.isUsable();

      if (!tokenValidation) {
        const error = new ConflictException(
          tokenValidation.validationErrors.join('. '),
        );
        throw this._encapsulatedErrorService.formatError(
          this._logger,
          new EncapsulatedError(error, error),
        );
      }

      try {
        user = await this.registerAccount({
          email: dto.email,
          password: dto.password,
          isActive: true,
        });
      } catch (err) {
        throw this._encapsulatedErrorService.formatError(this._logger, err);
      }

      try {
        await entityManager.save(AuthTokenEntity, {
          ...token,
          active: false,
          usedAt: new Date(),
        });
      } catch (err) {
        throw this._encapsulatedErrorService.formatError(this._logger, err);
      }
    });

    const event: IUserCreatedEvent = {
      subject: ApplicationEventSubject.UserCreated,
      payload: {
        email: user.email,
        id: user.userId,
      },
    };
    this._applicationEventsService.emit(event);

    return {
      data: user,
    };
  }
}
