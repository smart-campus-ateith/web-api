import {
  Body,
  ConflictException,
  Controller,
  Get,
  InternalServerErrorException,
  Logger,
  NotFoundException,
  NotImplementedException,
  Post,
  Request,
  UseGuards,
} from '@nestjs/common';
import {
  ForgotPasswordDTO,
  InviteUserDTO,
  LoginInputDTO,
  LoginResponseDTO,
  RegisterInputDTO,
  RegisterWithTokenInputDTO,
  ResetPasswordDTO,
  VerifyAccountDTO,
} from './dto/user-auth.dto';
import { AuthService } from './services/auth.service';
import {
  EncapsulatedError,
  EncapsulatedErrorService,
  IResponse,
  IsDevModeGuard,
} from '../common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { IsUserAuthGuard, ValidJwtAuthGuard } from './guards';
import { AuthTokenService } from './services/auth-token.service';
import { UsersService } from '../users/services/users.service';
import { User } from '../users/entities/user.entity';
import { AuthTokenEntity } from './entities/auth-token.entity';
import { UserAccount } from './entities/user-account.entity';
import { UserAccountService } from './services/user-account.service';
import { UserAccountLocal } from './entities/user-account-local.entity';
import { UserAccountLocalService } from './services/user-account-local.service';
import * as querystring from 'querystring';
import { EmailService } from '../email/services/email.service';
import {
  AccountCreatedEmail,
  AuthInviteUserEmail,
  AuthResetPasswordEmail,
  EmailSubject,
  EmailType,
} from '../email/email.types';
import { ConfigService } from '@nestjs/config';
import { IsAdminGuard } from './guards/is-admin.guard';

@Controller('auth')
@ApiTags('auth')
export class AuthController {
  private readonly _logger = new Logger(`Auth/${AuthController.name}`);

  constructor(
    private readonly _authService: AuthService,
    private readonly _encapsulatedErrorService: EncapsulatedErrorService,
    private readonly _resetTokenService: AuthTokenService,
    private readonly _usersService: UsersService,
    private readonly _userAccountService: UserAccountService,
    private readonly _userAccountLocalService: UserAccountLocalService,
    private readonly _emailService: EmailService,
    private readonly _configService: ConfigService,
  ) {
    const env = this._configService.get('app').environment;
    if (env === 'dev') {
      setImmediate(() => {
        this._logger.warn(
          `${AuthController.name} started in dev mode and allows manual registration`,
        );
      });
    }
  }

  @UseGuards(IsDevModeGuard)
  @Post('register')
  async register(@Body() dto: RegisterInputDTO) {
    try {
      return await this._authService.registerAccount(dto);
    } catch (err) {
      this._encapsulatedErrorService.handleControllerError(this._logger, err);
    }
  }

  @Post('login')
  async login(
    @Body() dto: LoginInputDTO,
  ): Promise<IResponse<LoginResponseDTO>> {
    try {
      const userAccount = await this._authService.verifyUserCredentials(dto);
      const token = this._authService.generateToken(userAccount);
      const response: LoginResponseDTO = {
        userAccount,
        token,
      };

      return {
        data: response,
      };
    } catch (err) {
      this._encapsulatedErrorService.handleControllerError(this._logger, err);
    }
    throw new NotImplementedException();
  }

  @ApiBearerAuth()
  @UseGuards(ValidJwtAuthGuard, IsUserAuthGuard)
  @Get('info')
  async info(@Request() req) {
    return {
      data: req.user,
    };
  }

  @Post('logout')
  async logout() {
    throw new NotImplementedException();
  }

  @Post('password-reset')
  async resetPassword(@Body() body: ResetPasswordDTO) {
    let token: AuthTokenEntity;
    let account: UserAccount;
    let localAccount: UserAccountLocal;

    try {
      const tokenEntityData = await this._resetTokenService.findOne({
        where: {
          token: body.token,
          additionalType: 'password-reset',
        },
        relations: ['owner'],
      });

      token = new AuthTokenEntity(tokenEntityData);
    } catch (err) {
      throw this._encapsulatedErrorService.formatError(this._logger, err);
    }

    if (!token) {
      throw new NotFoundException();
    }

    const usabilityValidation = token.isUsable();
    if (!usabilityValidation.isValid) {
      throw new ConflictException(
        usabilityValidation.validationErrors.join(','),
      );
    }

    try {
      account = await this._userAccountService.findOne({
        where: {
          isActive: true,
          email: token.owner.email,
        },
      });
    } catch (err) {
      throw this._encapsulatedErrorService.formatError(this._logger, err);
    }

    if (!account) {
      throw new ConflictException(
        `No active account found for ${token.owner.email}`,
      );
    }

    try {
      localAccount = await this._userAccountLocalService.findOne({
        where: {
          account: {
            id: account.id,
          },
        },
      });
    } catch (err) {
      throw this._encapsulatedErrorService.formatError(this._logger, err);
    }

    if (!localAccount) {
      throw new ConflictException(
        `Account for ${token.owner.email} is not configured to hava a password`,
      );
    }

    try {
      await this._userAccountLocalService.resetPasswordWithToken({
        token: token,
        newPassword: body.password,
        localAccount: localAccount,
      });
    } catch (err) {
      throw this._encapsulatedErrorService.formatError(this._logger, err);
    }

    return {
      data: {
        status: 'ok',
      },
    };
  }

  @Post('forgot-password')
  async forgotPassword(@Body() body: ForgotPasswordDTO) {
    let user: User;
    let token: AuthTokenEntity;

    try {
      user = await this._usersService.findOne({
        where: {
          email: body.email,
        },
      });
    } catch (err) {
      throw this._encapsulatedErrorService.formatError(
        this._logger,
        new EncapsulatedError(err, new InternalServerErrorException()),
      );
    }

    if (!user) {
      throw new ConflictException(`Account not found`);
    }

    try {
      token = await this._resetTokenService.save({
        owner: user,
        additionalType: 'password-reset',
      });
    } catch (err) {
      throw this._encapsulatedErrorService.formatError(
        this._logger,
        new EncapsulatedError(err, new InternalServerErrorException()),
      );
    }

    this._emailService.sendEmail<AuthResetPasswordEmail>({
      to: body.email,
      subject: EmailSubject.AuthResetPassword,
      template: EmailType.AuthResetPassword,
      context: {
        clientUrl: this._configService.get('app').clientURL,
        assetsPath: this._configService.get('app').assetsPath,
        clientPath: '#/login',
        token: token.token,
        clientQuery: querystring.encode({
          state: 'reset-password',
          token: token.token,
        }),
      },
    });

    return {
      data: {
        status: 'ok',
        expirationDate: token.expirationDate,
      },
    };
  }

  @UseGuards(IsDevModeGuard)
  @Post('verify')
  async verifyAccount(@Body() dto: VerifyAccountDTO) {
    let token: AuthTokenEntity;
    let account: UserAccount;

    try {
      const tokenEntityData = await this._resetTokenService.findOne({
        where: {
          token: dto.token,
          additionalType: 'verification',
        },
        relations: ['owner'],
      });

      token = new AuthTokenEntity(tokenEntityData);
    } catch (err) {
      throw this._encapsulatedErrorService.formatError(this._logger, err);
    }

    if (!token) {
      throw new NotFoundException();
    }

    const usabilityValidation = token.isUsable();
    if (!usabilityValidation.isValid) {
      throw new ConflictException(
        usabilityValidation.validationErrors.join(','),
      );
    }

    try {
      account = await this._userAccountService.findOne({
        where: {
          isActive: true,
          email: token.owner.email,
        },
      });
    } catch (err) {
      throw this._encapsulatedErrorService.formatError(this._logger, err);
    }

    if (!account) {
      throw new ConflictException(
        `No active account found for ${token.owner.email}`,
      );
    }

    return token;
  }

  @UseGuards(ValidJwtAuthGuard, IsUserAuthGuard, IsAdminGuard)
  @Post('invite-user')
  async inviteUser(@Request() req, @Body() dto: InviteUserDTO) {
    const result = await this._authService.inviteUser(req.user, dto.email);

    this._emailService.sendEmail<AuthInviteUserEmail>({
      to: dto.email,
      subject: EmailSubject.AuthInviteUser,
      template: EmailType.AuthInviteUser,
      context: {
        clientUrl: this._configService.get('app').clientURL,
        assetsPath: this._configService.get('app').assetsPath,
        clientPath: '#/login',
        clientQuery: querystring.encode({
          state: 'register',
          token: result.token.token,
          email: dto.email,
        }),
      },
    });

    return {
      data: 'ok',
    };
  }

  @Post('token-registration')
  async validateRegistrationToken(@Body() dto: RegisterWithTokenInputDTO) {
    const result = await this._authService.registerWithToken(dto);
    this._emailService.sendEmail<AccountCreatedEmail>({
      to: dto.email,
      subject: EmailSubject.AuthAccountCreated,
      template: EmailType.AuthAccountCreated,
      context: {
        clientUrl: this._configService.get('app').clientURL,
        assetsPath: this._configService.get('app').assetsPath,
      },
    });
    return result;
  }
}
