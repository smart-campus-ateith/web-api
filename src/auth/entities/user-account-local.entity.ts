import { Column, Entity, JoinColumn, OneToOne } from 'typeorm';
import { Thing } from '../../common';
import { UserAccount } from './user-account.entity';

/**
 *
 * @class UserAccountLocal
 * @description A user account that is managed locally
 *
 * @property {string} password The password that is associated with that account
 * @property {string} salt The salt used when the password was created
 * @property {UserAccount} account The parent user account
 *
 */
@Entity({
  name: 'Auth_UserAccountLocal',
})
export class UserAccountLocal extends Thing {
  @Column({
    nullable: false,
  })
  password: string;

  @OneToOne(() => UserAccount)
  @JoinColumn()
  account: UserAccount;
}
