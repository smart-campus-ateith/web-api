import { OwnedThing } from '../../users/entities/owned-thing.entity';
import { Column, DeepPartial, Entity } from 'typeorm';
import { EntityValidationResult } from '../../common';
import * as crypto from 'crypto';
import { Exclude } from 'class-transformer';

@Entity('Auth_AuthTokenEntity')
export class AuthTokenEntity extends OwnedThing {
  constructor(params?: DeepPartial<AuthTokenEntity>) {
    super(params);

    this.token = params?.token ?? crypto.randomBytes(32).toString('hex');
    this.active = params?.active;
    this.usedAt = params?.usedAt as Date;
    this.additionalType = params?.additionalType;
    this.expirationDate = params?.expirationDate as Date;
  }

  @Column({
    enum: ['verification', 'password-reset'],
  })
  additionalType: 'verification' | 'password-reset' | 'registration';

  @Exclude()
  @Column()
  token: string;

  @Column()
  expirationDate: Date;

  @Column({
    default: true,
  })
  active: boolean;

  @Column({
    nullable: true,
  })
  usedAt?: Date;

  isUsable() {
    const validationErrors = [];

    if (!this.token.length) {
      validationErrors.push('The token has invalid format');
    }
    if (this.expirationDate < new Date()) {
      validationErrors.push('The token has expired');
    }
    if (Boolean(this.usedAt)) {
      validationErrors.push('The token has already been used');
    }

    if (!this.active) {
      validationErrors.push('The token is not active');
    }

    return {
      isValid: validationErrors.length === 0,
      validationErrors: validationErrors,
    };
  }

  validate(): EntityValidationResult {
    const validation = super.validate();

    if (!this.token || this.token.length !== 64) {
      validation.validationErrors.push('Invalid token length');
    }

    return {
      validationErrors: validation.validationErrors,
      isValid: validation.validationErrors.length === 0,
    };
  }
}
