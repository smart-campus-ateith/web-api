import {
  Body,
  ClassSerializerInterceptor,
  ConflictException,
  Controller,
  Get,
  Logger,
  NotFoundException,
  Post,
  Query,
  Request,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { IsUserAuthGuard, ValidJwtAuthGuard } from '../guards';
import { IsAdminGuard } from '../guards/is-admin.guard';
import {
  BaseController,
  EncapsulatedError,
  EncapsulatedErrorService,
  EntityIdentifierDTO,
  IResponse,
  PaginationQueryDto,
  QueryService,
} from '../../common';
import { AuthTokenEntity } from '../entities/auth-token.entity';
import { AuthTokenService } from '../services/auth-token.service';
import { ApiBearerAuth, ApiExcludeEndpoint, ApiTags } from '@nestjs/swagger';
import { EmailService } from '../../email/services/email.service';
import {
  AuthInviteUserEmail,
  EmailSubject,
  EmailType,
} from '../../email/email.types';
import { ConfigService } from '@nestjs/config';
import * as querystring from 'querystring';

@Controller('user-invitation')
@ApiTags('user-invitation')
@ApiBearerAuth()
@UseGuards(ValidJwtAuthGuard, IsUserAuthGuard, IsAdminGuard)
@UseInterceptors(ClassSerializerInterceptor)
export class UserInvitationController extends BaseController<AuthTokenEntity> {
  constructor(
    protected _authTokenService: AuthTokenService,
    protected _encapsulatedErrorService: EncapsulatedErrorService,
    protected _queryService: QueryService,
    private readonly _emailService: EmailService,
    private readonly _configService: ConfigService,
  ) {
    super(
      _authTokenService,
      _queryService,
      _encapsulatedErrorService,
      new Logger(UserInvitationController.name),
    );
  }

  @ApiExcludeEndpoint()
  @Get()
  async get(
    @Request() req,
    @Query() paginationQuery: PaginationQueryDto,
  ): Promise<IResponse<AuthTokenEntity[]>> {
    return super.get(
      req,
      paginationQuery,
      {},
      {
        where: {
          additionalType: 'registration',
        },
        order: {
          createdAt: 'DESC',
        },
      },
      ['name', 'description'],
    );
  }

  @Post('send-email')
  async sendInvitationEmail(@Body() dto: EntityIdentifierDTO) {
    let token: AuthTokenEntity;
    try {
      token = await this._authTokenService.findOne({
        where: {
          identifier: dto.identifier,
        },
      });
    } catch (err) {
      throw this._encapsulatedErrorService.formatError(
        this._logger,
        new EncapsulatedError(err, err),
      );
    }

    if (!token) {
      throw this._encapsulatedErrorService.formatError(
        this._logger,
        new NotFoundException(`Token ${dto.identifier} was not found`),
      );
    }

    const usability = token.isUsable();

    if (!usability.isValid) {
      throw this._encapsulatedErrorService.formatError(
        this._logger,
        new ConflictException(
          `Token ${
            dto.identifier
          } is not active. ${usability.validationErrors.join()}`,
        ),
      );
    }

    this._emailService.sendEmail<AuthInviteUserEmail>({
      to: token.description,
      subject: EmailSubject.AuthInviteUser,
      template: EmailType.AuthInviteUser,
      context: {
        clientUrl: this._configService.get('app').clientURL,
        assetsPath: this._configService.get('app').assetsPath,
        clientPath: '#/login',
        clientQuery: querystring.encode({
          state: 'register',
          token: token.token,
          email: token.description,
        }),
      },
    });

    return {
      data: {
        status: 'ok',
      },
    };
  }
}
