import { Module } from '@nestjs/common';
import {
  CommonModule,
  IotWorkerJobsQueueEnum,
  IoTWorkerResultsQueueEnum,
} from '../common';
import { UsersModule } from '../users/users.module';
import { IoTTaskService } from './services/iot-task/iot-task.service';
import { ConfigModule } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import {
  IoTTaskEntity,
  IoTTaskCheckEntity,
  IoTTaskCheckThresholdEntity,
} from './entities';
import { IoTTaskCheckService } from './services/iot-task-check/iot-task-check.service';
import { IoTTaskCheckThresholdService } from './services/iot-check-threshold/iot-task-check-threshold.service';
import { BullModule } from '@nestjs/bullmq';
import { AlertCheckProcessor } from './processors/alert-check/alert-check.processor';
import { IoTTaskController } from './controllers/iot-task-controller';

@Module({
  imports: [
    CommonModule,
    ConfigModule,
    UsersModule,
    TypeOrmModule.forFeature([
      IoTTaskEntity,
      IoTTaskCheckEntity,
      IoTTaskCheckThresholdEntity,
    ]),
    BullModule.registerQueue({
      name: IoTWorkerResultsQueueEnum.Current,
    }),
    BullModule.registerQueue({
      name: IotWorkerJobsQueueEnum.Current,
    }),
  ],
  exports: [IoTTaskService, IoTTaskCheckService, IoTTaskCheckThresholdService],
  providers: [
    IoTTaskService,
    IoTTaskCheckService,
    IoTTaskCheckThresholdService,
    AlertCheckProcessor,
  ],
  controllers: [IoTTaskController],
})
export class IotTasksModule {}
