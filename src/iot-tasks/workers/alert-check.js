const { Worker, isMainThread, parentPort } = require('worker_threads');

// require('ts-node').register();
// require(path.resolve(__dirname, workerData.path));

parentPort.on('message', message => {
  console.log(`Worker ${process.pid}: Received task ${message.task.name}`);
  // Perform the task
  performTask(message.task)
    .then(() => {
      console.log('message: ', message);
      parentPort.postMessage(`[${process.pid}] Completed ${message.task.name}`);
    })
});
function performTask(task) {
  return new Promise((resolve) => {
    setTimeout(() => {
      return resolve();
    }, 5000);
  });
  // … operations to be performed to execute the task
}
