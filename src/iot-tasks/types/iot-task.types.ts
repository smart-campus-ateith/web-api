export enum IoTTaskType {
  AlertCheckThreshold = 'AlertCheckThreshold',
}

export enum IoTTaskConfigurationType {
  AlertCheck = 'AlertCheck',
}

export enum IoTTaskConfigurationDetailsType {
  ThresholdCheck = 'ThresholdCheck',
}
