import { Column, DeepPartial, Entity } from 'typeorm';
import { IoTTaskThresholdCheckCondition } from '../types';
import { EntityValidationResult } from '../../common';
import { IoTTaskConfigurationDetailsEntity } from './iot-task-configuration-details.entity';

/**
 *
 * @class IoTTaskCheckThresholdEntity
 * @extends IoTTaskConfigurationDetailsEntity
 * @description Holds information regarding an iot check task for a threshold
 *
 * @property {string} additionalType An identifier for the specific task
 * @property {number} warningThreshold The threshold to pass for a warning status
 * @property {number} criticalThreshold The threshold to pass for a critical status
 * @property {IoTTaskThresholdCheckCondition} condition The condition to be satisfied
 *
 */
@Entity('IoTTask_IoTTaskCheckThresholdEntity')
export class IoTTaskCheckThresholdEntity extends IoTTaskConfigurationDetailsEntity {
  constructor(props?: DeepPartial<IoTTaskCheckThresholdEntity>) {
    super(props);

    if (props) {
      this.warningThreshold = props.warningThreshold;
      this.criticalThreshold = props.criticalThreshold;
      this.condition = props.condition;
    }

    this.additionalType = 'IoTTaskCheckThresholdEntity';
  }

  @Column({
    default: 'IoTTaskCheckThresholdEntity',
    type: 'text',
  })
  additionalType = 'IoTTaskCheckThresholdEntity';

  @Column({
    nullable: true,
  })
  warningThreshold?: number;

  @Column({
    nullable: false,
  })
  criticalThreshold?: number;

  @Column({
    nullable: false,
    default: IoTTaskThresholdCheckCondition.Greater,
    enum: IoTTaskThresholdCheckCondition,
  })
  condition: IoTTaskThresholdCheckCondition;

  validate(): EntityValidationResult {
    const validation = super.validate();

    if (isNaN(this.criticalThreshold)) {
      validation.validationErrors.push(
        `Property 'criticalThreshold' must be a number. Got ${this.criticalThreshold}`,
      );
    }

    if (!this.condition) {
      validation.validationErrors.push(
        `Property 'condition' must be 'lower' or 'greater'. Got ${this.condition}`,
      );
    }

    if (this.condition === IoTTaskThresholdCheckCondition.Greater) {
      if (this.warningThreshold >= this.criticalThreshold) {
        validation.validationErrors.push(
          `Property 'warningThreshold' must be lower than 'criticalThreshold' for condition ${this.condition}`,
        );
      }
    } else if (this.condition === IoTTaskThresholdCheckCondition.Lower) {
      if (this.warningThreshold <= this.criticalThreshold) {
        validation.validationErrors.push(
          `Property 'warningThreshold' must be greater than 'criticalThreshold' for condition ${this.condition}`,
        );
      }
    }

    return {
      isValid: validation.validationErrors.length === 0,
      validationErrors: validation.validationErrors,
    };
  }
}
