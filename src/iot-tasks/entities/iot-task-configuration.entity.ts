import { OwnedThing } from '../../users/entities/owned-thing.entity';

export abstract class IoTTaskConfigurationEntity extends OwnedThing {
  constructor(params) {
    super(params);
  }
}
