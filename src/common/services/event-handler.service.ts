import { Logger } from '@nestjs/common';
import {
  ApplicationEventSubject,
  IApplicationEventPayload,
} from '../interfaces';
import { ApplicationEventsService } from './application-events.service';

export class EventHandlerService {
  constructor(protected readonly _logger: Logger) {}

  logEvent(subject: ApplicationEventSubject, event: IApplicationEventPayload) {
    this._logger.verbose(
      ApplicationEventsService.formatReceiverLog(subject, event),
    );
  }
}
