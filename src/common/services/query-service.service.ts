import { IPaginationQuery } from '../interfaces';
import { PaginationQueryDto, ResourceSearchQueryDTO } from '../dto';
import { Raw } from 'typeorm';

interface DefaultPagination {
  page?: number;
  perPage?: number;
}

export class QueryService {
  resolvePagination(
    query: PaginationQueryDto,
    defaultPagination?: DefaultPagination,
  ): IPaginationQuery {
    const page = isNaN(query?.page)
      ? defaultPagination?.page ?? 1
      : Number(query?.page);
    const perPage = isNaN(query?.perPage)
      ? defaultPagination?.perPage ?? 10
      : query.perPage;
    const skip = perPage * (page - 1);

    return {
      page,
      perPage,
      skip,
      take: perPage,
    };
  }

  resolveSearchQuery(query: ResourceSearchQueryDTO, fields: string[]) {
    if (!query?.q) {
      return {};
    }

    const q = decodeURIComponent(query.q).trim();

    const searchFactors = [];
    for (const field of fields) {
      const searchParam: Record<string, unknown> = {};
      searchParam[field] = Raw(
        (alias) => ` LOWER(${alias}) LIKE LOWER('%${q}%')`,
      );
      searchFactors.push(searchParam);
    }

    return {
      q,
      searchFactors,
    };
  }
}
