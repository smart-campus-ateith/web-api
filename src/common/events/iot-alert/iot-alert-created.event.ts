import { ApplicationEventSubject, IApplicationEvent } from '../../interfaces';

export interface IIoTAlertCreatedEvent extends IApplicationEvent {
  subject: ApplicationEventSubject.IoTAlertCreated;
  payload: {
    status: 'error' | 'normal' | 'warning' | 'critical';
    prevStatus?: 'error' | 'normal' | 'warning' | 'critical';
    alertConfiguration: string;
    alert: string;
    value: number;
  };
}
