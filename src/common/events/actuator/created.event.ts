import {
  ApplicationEventSubject,
  IApplicationEvent,
  IApplicationEventPayload,
  IoTValue,
} from '../../interfaces';

/**
 *
 * @interface IActuatorCreatedEventPayload
 * @description The payload of a IActuatorCreatedEvent
 *
 * @property {string} actuator The public identifier of the IoTActuator entity instance
 * @property {string} name The name of the IoTActuator instance
 * @property {string} owner The public identifier of the IoTActuator owner
 * @property {string} parentDevice The public identifier of the IoTDevice which is parent of the IoTActuator
 * @property {IoTValue} valueType The valueType of the IoTActuator
 * @property {string} reportFormat The report format of the IoTActuator
 * @property {number} lowerLimit The lower limit of the IoTActuator
 * @property {number} upperLimit The upper limit of the IoTActuator
 *
 */
interface IActuatorCreatedEventPayload extends IApplicationEventPayload {
  actuator: string;
  name?: string;
  owner: string;
  parentDevice: string;
  valueType: IoTValue;
  reportFormat: 'json' | 'raw';
  lowerLimit?: number;
  upperLimit?: number;
}

/**
 *
 * @interface IActuatorCreatedEvent
 * @description Describes the creation of an IoTActuator
 *
 * @property {string} subject The event subject
 * @property {IActuatorCreatedEventPayload} The payload
 *
 */
export interface IActuatorCreatedEvent extends IApplicationEvent {
  subject: ApplicationEventSubject.ActuatorCreated;
  payload: IActuatorCreatedEventPayload;
}
