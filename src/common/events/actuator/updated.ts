import {
  ApplicationEventSubject,
  IApplicationEvent,
  IApplicationEventPayload,
} from '../../interfaces';

/**
 *
 * @interface IActuatorUpdatedEventPayload
 * @description The payload of an IActuatorUpdatedEvent
 *
 * @property {string} actuator The public identifier of the IoTActuator entity instance
 * @property {string} name The name of the IoTActuator instance
 * @property {string} reportFormat The report format of the IoTActuator
 * @property {number} lowerLimit The lower limit of the IoTActuator
 * @property {number} upperLimit The upper limit of the IoTActuator
 * @property {number} version The version of the instance
 *
 */
interface IActuatorUpdatedEventPayload extends IApplicationEventPayload {
  actuator: string;
  name?: string;
  reportFormat?: 'json' | 'raw';
  lowerLimit?: number;
  upperLimit?: number;
  version: number;
}

/**
 *
 * @interface IActuatorUpdatedEvent
 * @description Describes the update of an IoTActuator
 *
 * @property {string} subject The event subject
 * @property {IActuatorUpdatedEventPayload} The payload
 *
 */
export interface IActuatorUpdatedEvent extends IApplicationEvent {
  subject: ApplicationEventSubject.ActuatorUpdated;
  payload: IActuatorUpdatedEventPayload;
}
