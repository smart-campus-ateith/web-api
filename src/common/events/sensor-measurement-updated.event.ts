import { ApplicationEventSubject, IApplicationEvent } from '../interfaces';

export interface SensorMeasurementUpdatedEvent extends IApplicationEvent {
  subject: ApplicationEventSubject.SensorMeasurementUpdated;
  payload: {
    measurement: string;
    isActive?: boolean;
    isProduction?: boolean;
    version: number;
  };
}
