import { ApplicationEventSubject, IApplicationEvent } from '../interfaces';

export interface IIoTDeviceCreatedEvent extends IApplicationEvent {
  subject: ApplicationEventSubject.IoTDeviceCreated;
  payload: {
    identifier: string;
    ownerId: number;
  };
}
