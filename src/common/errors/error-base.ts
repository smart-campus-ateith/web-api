export class EncapsulatedError extends Error {
  error: Error;
  presentableError: Error;

  constructor(error: Error, presentableError: Error) {
    super(error?.message);
    this.error = error;
    this.presentableError = presentableError;
  }
}
