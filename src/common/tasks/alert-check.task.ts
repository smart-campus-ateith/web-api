import { Task } from './tasks';

export interface AlertCheckTask {
  queue: Task.AlertCheck;
  body: {
    identifier: string;
    measurement: string;
    task: string;
  };
}
