export enum IoTDeviceAccessTokenType {
  HTTPAccess = 'http-access',
  MQTTAccess = 'mqtt-access',
}
