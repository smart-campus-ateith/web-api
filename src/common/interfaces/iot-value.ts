export enum IoTValueType {
  BOOLEAN = 'boolean',
  INT = 'int',
  FLOAT = 'float',
  STRING = 'string',
}

export type IoTValue = 'int' | 'float' | 'boolean' | 'string';
