import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsUUID } from 'class-validator';

/**
 *
 * @class EntityIdentifierDTO
 * @description The query parameters of
 *
 * @property {string} identifier The identifier of the resource
 *
 */
export class EntityIdentifierDTO {
  @ApiProperty()
  @IsNotEmpty()
  @IsUUID()
  identifier: string;
}
