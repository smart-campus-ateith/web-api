import {
  Controller,
  ForbiddenException,
  InternalServerErrorException,
  Logger,
  NotFoundException,
  Param,
  Query,
  Request,
} from '@nestjs/common';
import { EntityBase } from '../entities';
import {
  EncapsulatedErrorService,
  EntityService,
  QueryService,
} from '../services';
import { EncapsulatedError } from '../errors';
import { FindManyOptions, FindOneOptions, FindOptionsWhere } from 'typeorm';
import {
  EntityIdentifierDTO,
  PaginationQueryDto,
  ResourceSearchQueryDTO,
} from '../dto';
import { IResponse } from '../interfaces';

@Controller()
export class BaseController<T extends EntityBase> {
  constructor(
    protected readonly _entityService: EntityService<T>,
    protected readonly _queryService: QueryService,
    protected readonly _encapsulatedErrorService: EncapsulatedErrorService,
    protected readonly _logger: Logger,
  ) {}

  async get(
    @Request() req,
    @Query() pagination: PaginationQueryDto,
    @Query() query: ResourceSearchQueryDTO,
    findManyOptions: FindManyOptions<T>,
    searchFields?: string[],
  ): Promise<IResponse<T[]>> {
    const { take, skip, page, perPage } =
      this._queryService.resolvePagination(pagination);

    let resourceList: T[];
    let resourceCount: number;

    const { searchFactors } = this._queryService.resolveSearchQuery(
      query,
      searchFields,
    );

    const whereClauseWithSearch =
      this._entityService.formatFindOptionsWithSearch(
        findManyOptions.where as FindOptionsWhere<T>,
        searchFactors,
      );

    try {
      [resourceList, resourceCount] = await this._entityService.findAndCount({
        where: whereClauseWithSearch,
        order: findManyOptions.order,
        relations: findManyOptions.relations,
        take,
        skip,
      });
    } catch (err) {
      throw this._encapsulatedErrorService.formatError(
        this._logger,
        new EncapsulatedError(err, new InternalServerErrorException()),
      );
    }

    return {
      page,
      perPage,
      total: resourceCount,
      data: resourceList,
    };
  }

  async getOne(
    @Request() req,
    @Param() params: EntityIdentifierDTO,
    options: FindOneOptions<T>,
  ): Promise<IResponse<T>> {
    let resource: T;

    try {
      resource = await this._entityService.findOne(options);
    } catch (err) {
      throw this._encapsulatedErrorService.formatError(
        this._logger,
        new EncapsulatedError(err, new InternalServerErrorException()),
      );
    }

    if (!resource) {
      const notFoundError = new NotFoundException(
        `${this._entityService.name} with identifier ${params.identifier} was not found`,
      );
      throw this._encapsulatedErrorService.formatError(
        this._logger,
        new EncapsulatedError(notFoundError, notFoundError),
      );
    }

    return {
      data: resource,
    };
  }

  async remove(
    @Request() req,
    @Param() params: EntityIdentifierDTO,
    findOptions: FindManyOptions<T>,
  ) {
    let resource: T;

    try {
      resource = await this._entityService.findOne({
        where: findOptions.where,
        relations: findOptions.relations,
        withDeleted: false,
      });
    } catch (err) {
      throw this._encapsulatedErrorService.formatError(
        this._logger,
        new EncapsulatedError(err, new InternalServerErrorException()),
      );
    }

    if (!resource) {
      const error = new NotFoundException(
        `${this._entityService.name} ${params?.identifier} was not found`,
      );
      throw this._encapsulatedErrorService.formatError(
        this._logger,
        new EncapsulatedError(error, error),
      );
    } else if (
      typeof (resource as any)?.owner?.id !== 'undefined' &&
      (resource as any)?.owner?.id !== req.user.id
    ) {
      const error = new ForbiddenException(
        `${this._entityService.name} ${params?.identifier} is not deletable`,
      );
      throw this._encapsulatedErrorService.formatError(
        this._logger,
        new EncapsulatedError(error, error),
      );
    } else {
      await this._entityService.softRemove((resource as any).id);
      const removed = await this._entityService.findOne({
        where: findOptions.where,
        withDeleted: true,
      });

      return {
        data: removed,
      };
    }
  }

  getThrowable(originalError: Error, presentableError?: Error) {
    this._logger.error(originalError.stack ?? originalError);

    if (presentableError) {
      return presentableError;
    } else {
      return new InternalServerErrorException();
    }
  }

  filterUpdatableFields(dto: unknown, editableFields: string[]) {
    const updatedFields: Partial<T> = {};

    for (const field of editableFields) {
      if (dto.hasOwnProperty(field)) {
        updatedFields[field] = dto[field];
      }
    }

    return updatedFields;
  }
}
