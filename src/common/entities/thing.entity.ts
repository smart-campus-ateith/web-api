import {
  Column,
  CreateDateColumn,
  DeepPartial,
  DeleteDateColumn,
  Generated,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
  VersionColumn,
} from 'typeorm';
import { EntityBase } from './entity-base';
import { Exclude } from 'class-transformer';
import { EntityValidationResult } from '../interfaces';

/**
 *
 * @abstract
 * @class Thing
 * @description Holds common fields for most, if not all models
 * https://schema.org/Thing
 *
 * @property {number} id The machine-friendly unique identifier for the object
 * @property {string} uuid A public sharable identifier for the object
 * @property {string} additionalName A name to identify an object regardless of its id
 * @property {string} description A short description about the object
 * @property {Date} createdAt The date the the object created in the database
 * @property {Date} updatedAt The date that the object updated
 * @property {number} version The current version of the object
 *
 */
export abstract class Thing extends EntityBase {
  constructor(props?: DeepPartial<Thing>) {
    super();

    if (props) {
      this.id = props.id;
      this.identifier = props.identifier;
      this.deletedDate = props.deletedDate as Date;
      this.updatedAt = props.updatedAt as Date;
      this.createdAt = props.createdAt as Date;
      this.version = props.version;
      this.description = props.description;
      this.additionalName = props.additionalName;
    }
  }

  @Exclude()
  @PrimaryGeneratedColumn()
  id?: number;

  @Column({
    unique: true,
  })
  @Generated('uuid')
  identifier?: string;

  @Column({
    nullable: true,
  })
  description?: string;

  @Column({
    nullable: true,
  })
  additionalName?: string;

  @CreateDateColumn()
  createdAt?: Date;

  @UpdateDateColumn()
  updatedAt?: Date;

  @DeleteDateColumn()
  deletedDate?: Date;

  @VersionColumn()
  version?: number;

  validate(): EntityValidationResult {
    return {
      isValid: true,
      validationErrors: [],
    };
  }
}
