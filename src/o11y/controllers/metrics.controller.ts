import { Controller, Get, Header, Res } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import * as PromClient from 'prom-client';

const register = new PromClient.Registry();

PromClient.collectDefaultMetrics({
  prefix: 'node_',
  gcDurationBuckets: [0.001, 0.01, 0.1, 1, 2, 5],
  register,
});

@Controller('metrics')
@ApiTags('metrics')
export class MetricsController {
  @Get()
  @Header('Content-Type', register.contentType)
  async get() {
    return await register.metrics();
  }
}
